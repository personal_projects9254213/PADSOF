/**
 * Class Playlist test
 * @author <a href="mailto:nicolas.serranos@estudiante.uam.es">Nicolas Serrano</a>
 * @author <a href="mailto:miguela.luque@estudiante.uam.es">Miguel Angel Luque</a>
 * @author <a href="mailto:blanca.mercado@estudiante.uam.es">Blanca Mercado</a>
 * Practice group: 2291
 */
package es.uam.eps.sadp.p3;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

/**
 * The Class PlaylistTest.
 */
public class PlaylistTest {

	private Playlist p1;
	private Playlist p2;
	private Song s1;
	private Song s2;
	private Registered author;
	private Album a1;
	
	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		author = new Registered("name1", "nick1", "password1", LocalDate.of(1999,6,28));
		a1 = new Album("a1", author);
		s1 = new Song("song1.mp3", "song1", author);
		s2 = new Song("song12.mp3", "song2", author);
		p1 = new Playlist("title1", s1, author);
		p2 = new Playlist("title1", s2, author);
	}
	
	/**
	 * Test method for {@link es.uam.eps.sadp.p3.Playlist#getTitle()}.
	 */
	@Test
	public void testGetTitle() {
		assertEquals("title1",p1.getTitle());
	}

	/**
	 * Test method for {@link es.uam.eps.sadp.p3.Playlist#getSongs()}.
	 */
	@Test
	public void testGetSongs() {
		List<Song> songs = new ArrayList<>();
		songs.add(s1);
		assertEquals(songs,p1.getSongs());
	}

	/**
	 * Test method for {@link es.uam.eps.sadp.p3.Playlist#addToPlaylist(es.uam.eps.sadp.p3.Song)}.
	 */
	@Test
	public void testAddToPlaylistSong() {
		List<Song> songs = new ArrayList<>();
		p1.addToPlaylist(s2);
		songs.add(s1);
		songs.add(s2);
		assertEquals(songs,p1.getSongs());
		p1.quitFromPlaylist(s2);
	}

	/**
	 * Test method for {@link es.uam.eps.sadp.p3.Playlist#addToPlaylist(es.uam.eps.sadp.p3.Album)}.
	 */
	@Test
	public void testAddToPlaylistAlbum() {
		List<Song> songs = new ArrayList<>();
		a1.addSong(s2);
		p1.addToPlaylist(a1);
		songs.add(s1);
		songs.add(s2);
		assertEquals(songs,p1.getSongs());
		p1.quitFromPlaylist(s2);
	}

	/**
	 * Test method for {@link es.uam.eps.sadp.p3.Playlist#addToPlaylist(es.uam.eps.sadp.p3.Playlist)}.
	 */
	@Test
	public void testAddToPlaylistPlaylist() {
		List<Song> songs = new ArrayList<>();
		songs.add(s1);
		songs.add(s2);
		p1.addToPlaylist(p2);
		assertEquals(songs,p1.getSongs());
		p1.quitFromPlaylist(s2);
	}

	/**
	 * Test method for {@link es.uam.eps.sadp.p3.Playlist#quitFromPlaylist(es.uam.eps.sadp.p3.Song)}.
	 */
	@Test
	public void testQuitFromPlaylist() {
		List<Song> songs = new ArrayList<>();
		p1.addToPlaylist(s2);
		songs.add(s1);
		songs.add(s2);
		assertEquals(songs,p1.getSongs());
		p1.quitFromPlaylist(s2);
		songs.remove(s2);
		assertEquals(songs,p1.getSongs());
	}
	

}
