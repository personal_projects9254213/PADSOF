/**
 * Class Album test
 * @author <a href="mailto:nicolas.serranos@estudiante.uam.es">Nicolas Serrano</a>
 * @author <a href="mailto:miguela.luque@estudiante.uam.es">Miguel Angel Luque</a>
 * @author <a href="mailto:blanca.mercado@estudiante.uam.es">Blanca Mercado</a>
 * Practice group: 2291
 */
package es.uam.eps.sadp.p3;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class AlbumTest {

	private Song s1;
	private Song s3;
	private Registered author;
	private Album album1;
	
	@Before
	public void setUp() throws Exception {
		author = new Registered("name1", "nick1", "password1", LocalDate.of(1999,6,28));
		album1 = new Album("album1", author);
		s1 = new Song("song1.mp3", "song1", author);
		s3 = new Song("song3.mp3", "song3", author);
		album1.addSong(s3);
	}

	/**
	 * Test method for {@link es.uam.eps.sadp.p3.Album#addSong()}.
	 */
	@Test
	public void testAddSong() {
		List<Song> songs = new ArrayList<>();
		album1.addSong(s1);
		songs.add(s3);
		songs.add(s1);
		assertEquals(songs,album1.getSongs());
		album1.deleteSong(s1);
	}
	
	/**
	 * Test method for {@link es.uam.eps.sadp.p3.Album#getTitle()}.
	 */
	@Test
	public void testGetTitle() {
		assertEquals("album1",album1.getTitle());
	}

	/**
	 * Test method for {@link es.uam.eps.sadp.p3.Album#getAuthor()}.
	 */
	@Test
	public void testGetAuthor() {
		assertEquals(author,album1.getAuthor());
	}

	/**
	 * Test method for {@link es.uam.eps.sadp.p3.Album#getSongs()}.
	 */
	@Test
	public void testGetSongs() {
		List<Song> songs = new ArrayList<>();
		songs.add(s3);
		assertEquals(songs,album1.getSongs());
	}

	/**
	 * Test method for {@link es.uam.eps.sadp.p3.Album#validate(java.lang.Boolean, java.lang.Boolean)}.
	 */
	@Test
	public void testValidate() {
		album1.addSong(s1);
		album1.validate(true, false,LocalDate.of(1999,6,28));
		assertTrue(s1.isValidate());
		assertFalse(s1.isExplicit());
		assertTrue(s3.isValidate());
		assertFalse(s3.isExplicit());
		assertEquals(LocalDate.of(1999,6,28),s1.getRejectDate());
		assertEquals(LocalDate.of(1999,6,28),s3.getRejectDate());
		album1.deleteSong(s1);
	}

	/**
	 * Test method for {@link es.uam.eps.sadp.p3.Album#deleteSong(es.uam.eps.sadp.p3.Song)}.
	 */
	@Test
	public void testDeleteSong() {
		List<Song> songs = new ArrayList<>();
		album1.addSong(s1);
		songs.add(s3);
		songs.add(s1);
		assertEquals(songs,album1.getSongs());
		album1.deleteSong(s1);
		songs.remove(s1);
		assertEquals(songs,album1.getSongs());
	}


}
