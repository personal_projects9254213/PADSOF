package es.uam.eps.sadp.p3;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

public class AdminTest {
	private Admin user;
	private Song song1;
	private Song song2;
	private Registered fo;	
	
	@Before
	public void setUp() {
		user = new Admin("Rex", "supersecret");
		fo = new Registered("name1", "nick1", "password1", LocalDate.of(1999,6,28));
		song1 = new Song("./testSong/song3.mp3","goodbunny",fo);
		song2 = new Song("./testSong/song4.mp3","goodbunny",fo);
	}
	@Test
	public final void testGetNick() {
		assertTrue(user.getNick()== "Rex");
	}

	@Test
	public final void testGetPassword() {
		assertTrue(user.getPassword() == "supersecret".hashCode());
	}
	@Test
	public void testPlay() {
		song1.validate(false, true, LocalDate.of(1999,6,28));
		song2.validate(true, false, LocalDate.of(2007,4,9));
		try{
			assertTrue(user.play(song1));
			assertTrue(user.play(song2));
		}
		catch(Exception e){
			fail();
		}
	}
}
