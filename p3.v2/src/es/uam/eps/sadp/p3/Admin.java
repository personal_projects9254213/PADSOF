package es.uam.eps.sadp.p3;

import java.io.FileNotFoundException;
import pads.musicPlayer.Mp3Player;
import pads.musicPlayer.exceptions.Mp3PlayerException;

/**
 * Class Admin
 * @author <a href="mailto:nicolas.serranos@estudiante.uam.es">Nicolas Serrano</a>
 * @author <a href="mailto:miguela.luque@estudiante.uam.es">Miguel Angel Luque</a>
 * @author <a href="mailto:blanca.mercado@estudiante.uam.es">Blanca Mercado</a>
 * Practice group: 2291
 */
public class Admin extends User {
	private static final long serialVersionUID = 1L;

	/**
     * Constructor of the class Admin
     * @param nick of the user
     * @param password of the user
     */
	public Admin(String nick, String password){
		super(nick, password);
	}

	@Override
	/**
     * Plays the song passed by argument
     * @param song song wanted to play
     * @return boolean saying if the song have been reproducted
     * @exception FileNotFoundException if file does not exist.
     * @exception Mp3PlayerException Thrown either when the file is not found, or is not valid mp3,
     * or when the Player could not be created
     * @exception InterruptedException
     */
	public boolean play(Song song) throws FileNotFoundException, Mp3PlayerException, InterruptedException {
		Mp3Player player = new Mp3Player();
		if(!Mp3Player.isValidMp3File(song.getPath())) {
			return false;
		}
		player.stop();
		player.add(song.getPath());
		player.play();
		Thread.sleep((long)Mp3Player.getDuration(song.getPath())*1000);
		return true;
	}
	
	/**
	 *stops the current play
	 */
	public void stop() {
    	player.stop();
    }
}
