package es.uam.eps.sadp.p3;

import java.io.FileNotFoundException;
import java.io.Serializable;
import java.time.LocalDate;

import pads.musicPlayer.Mp3Player;
import pads.musicPlayer.exceptions.Mp3PlayerException;

/**
 * Class Unregistered
 * @author <a href="mailto:nicolas.serranos@estudiante.uam.es">Nicolas Serrano</a>
 * @author <a href="mailto:miguela.luque@estudiante.uam.es">Miguel Angel Luque</a>
 * @author <a href="mailto:blanca.mercado@estudiante.uam.es">Blanca Mercado</a>
 * Practice group: 2291
 */
public class Unregistered extends User implements Serializable {
    private static final long serialVersionUID = 1L;
    public static final int MAXSONGS = 1000;
    private int listenedSongs;
    private LocalDate nextResetDate;

    /**
     * Constructor of the class Unregistered
     */
    public Unregistered(){
        this("Egg", "Dino");
    }

    /**
     * Constructor of the class Unregistered
     * @param nick of the user
     * @param password of the user
     */
    public Unregistered(String nick, String password){
        super(nick, password);
        listenedSongs = 0;
        nextResetDate = LocalDate.now().plusMonths(1).withDayOfMonth(1);
		
    }

    /**
	 * Gets the listenedSongs of that month
	 * @return the number of listened songs
	 */
    public int getListenedSongs(){
        if(!nextResetDate.isAfter(LocalDate.now())){
            listenedSongs = 0;
            nextResetDate = nextResetDate.plusMonths(1);
        }
        return listenedSongs;
    }

    public void setListenedSongs(int lstn){
        listenedSongs = lstn;
    }

    /**
     * Plays the song passed by argument
     * @param song song wanted to play
     * @return boolean saying if the song have been reproducted
     * @exception FileNotFoundException if file does not exist.
     * @exception Mp3PlayerException Thrown either when the file is not found, or is not valid mp3,
     * or when the Player could not be created
     * @exception InterruptedException
     */
    @Override
    public boolean play(Song song) throws FileNotFoundException, Mp3PlayerException, InterruptedException {
    	if(player == null) {
    		player = new Mp3Player();
    	}
        if(!Mp3Player.isValidMp3File(song.getPath()) ||song.isExplicit()){
            return false;
        } else if(getListenedSongs() < MAXSONGS) {
            setListenedSongs(getListenedSongs()+1);
            song.setReproductions(song.getReproductions()+1);
			player.stop();
            player.add(song.getPath());
            player.play();
            return true;
        } else {
            return false;
        }
    }
    
    /**
	 *stops the current play
	 */
    public void stop() {
    	player.stop();
    }
}
