/**
 * Class song test
 * @author <a href="mailto:nicolas.serranos@estudiante.uam.es">Nicolas Serrano</a>
 * @author <a href="mailto:miguela.luque@estudiante.uam.es">Miguel Angel Luque</a>
 * @author <a href="mailto:blanca.mercado@estudiante.uam.es">Blanca Mercado</a>
 * Practice group: 2291
 */
package es.uam.eps.sadp.p3;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;


/**
 * The Class SongTest.
 */
public class SongTest {

	private Song s1;
	private Song s2;
	private Song s3;
	private Registered author;	
	private Album album2;
	
	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		author = new Registered("name1", "nick1", "password1", LocalDate.of(1999,6,28));
		album2 = new Album("album2", author);
		s1 = new Song("song1.mp3", "song1", author);
		s2 = new Song("song1.mp3", "song1", author, album2);
		s3 = new Song("song3.mp3", "song3", author);
	}

	/**
	 * Test method for {@link es.uam.eps.sadp.p3.Song#getTitle()}.
	 */
	@Test
	public void testGetTitle() {
		assertEquals("song1",s1.getTitle());
	}

	/**
	 * Test method for {@link es.uam.eps.sadp.p3.Song#getAuthor()}.
	 */
	@Test
	public void testGetAuthor() {
		assertSame(author,s1.getAuthor());
	}

	/**
	 * Test method for {@link es.uam.eps.sadp.p3.Song#getPath()}.
	 */
	@Test
	public void testGetPath() {
		assertEquals("song1.mp3",s1.getPath());
	}

	/**
	 * Test method for {@link es.uam.eps.sadp.p3.Song#getReproductions()}.
	 */
	@Test
	public void testGetReproductions() {
		assertEquals(0,s1.getReproductions());
	}

	/**
	 * Test method for {@link es.uam.eps.sadp.p3.Song#isExplicit()}.
	 */
	@Test
	public void testIsExplicit() {
		s1.validate(false, true,LocalDate.of(1999,6,28));
		assertTrue(s1.isExplicit());
	}

	/**
	 * Test method for {@link es.uam.eps.sadp.p3.Song#isValidate()}.
	 */
	@Test
	public void testIsValidate() {
		s1.validate(false, true,LocalDate.of(1999,6,28));
		assertFalse(s1.isValidate());
	}

	/**
	 * Test method for {@link es.uam.eps.sadp.p3.Song#getValidate()}.
	 */
	@Test
	public void testGetValidate() {
		assertEquals(Song.ValidationType.INPROGRESS,s3.getValidate());
		s1.validate(false, true,LocalDate.of(1999,6,28));
		assertEquals(Song.ValidationType.FALSE,s1.getValidate());
    }
	
	/**
	 * Test method for {@link es.uam.eps.sadp.p3.Song#setValidate()}.
	 */
	@Test
	public void testSetValidate() {
		s1.setValidate(Song.ValidationType.INPROGRESS);
		assertEquals(Song.ValidationType.INPROGRESS,s1.getValidate());
		s1.setValidate(Song.ValidationType.FALSE);
		assertEquals(Song.ValidationType.FALSE,s1.getValidate());
    }
	
	/**
	 * Test method for {@link es.uam.eps.sadp.p3.Song#getAlbum()}.
	 */
	@Test
	public void testGetAlbum() {
		assertNull(s1.getAlbum());
		assertSame(album2,s2.getAlbum());
	}

	/**
	 * Test method for {@link es.uam.eps.sadp.p3.Song#getRejectDate()}.
	 */
	@Test
	public void testGetRejectDate() {
		s1.validate(true, false,LocalDate.of(1999,6,16));
		assertEquals(LocalDate.of(1999,6,16),s1.getRejectDate());
	}
	
	/**
	 * Test method for {@link es.uam.eps.sadp.p3.Song#setReproductions(long)}.
	 */
	@Test
	public void testSetReproductions() {
		s1.setReproductions(1);
		assertEquals(1,s1.getReproductions());
	}
	
	/**
	 * Test method for {@link es.uam.eps.sadp.p3.Song#setTitle(String)}.
	 */
	@Test
	public void testSetTitle() {
		s1.setTitle("titlenew");
		assertEquals("titlenew",s1.getTitle());
	}
	
	/**
	 * Test method for {@link es.uam.eps.sadp.p3.Song#setPath(String)}.
	 */
	@Test
	public void testSetPath() {
		s1.setPath("pathnew");
		assertEquals("pathnew",s1.getPath());
	}

	/**
	 * Test method for {@link es.uam.eps.sadp.p3.Song#validate(java.lang.Boolean, java.lang.Boolean)}.
	 */
	@Test
	public void testValidate() {
		s1.validate(true, false,LocalDate.of(1999,6,28));
		assertTrue(s1.isValidate());
		assertFalse(s1.isExplicit());
		assertEquals(LocalDate.of(1999,6,28),s1.getRejectDate());
	}
	

}
