package es.uam.eps.sadp.p4.vista.figuras;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComboBox;

import es.uam.eps.sadp.p3.System;
import es.uam.eps.sadp.p4.vista.newPlaylistPopUp;

public class SearchComboBox extends JComboBox<String>{
	final static String[] options = { "User", "Song", "Album"};
	
	public SearchComboBox() {
		super(options);
		setBounds(0,0,70,20);
		setRenderer(new MyComboBoxRenderer("Filter"));
		setSelectedIndex(-1);
		
	}

}
