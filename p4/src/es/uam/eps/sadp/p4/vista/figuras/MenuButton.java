package es.uam.eps.sadp.p4.vista.figuras;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Shape;
import java.awt.geom.RoundRectangle2D;

import javax.swing.JButton;
import javax.swing.SwingConstants;

public class MenuButton extends JButton {

	public MenuButton(String text){
		super(text);
		Dimension size = getPreferredSize();
		size.width = size.height = Math.max(size.width, size.height);
		setPreferredSize(size);
		setBorder(null);
		setContentAreaFilled(false);
		setFont(new Font("SansSerif", Font.BOLD, 16));
		setHorizontalAlignment(SwingConstants.LEFT);
	}
	
}
