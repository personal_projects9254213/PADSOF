package es.uam.eps.sadp.p4.vista.figuras;

import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

public class MyComboBoxRenderer extends JLabel implements ListCellRenderer {
    private String title;

    public MyComboBoxRenderer(String title) {
        this.title = title;
    }

    @Override
    public MyComboBoxRenderer getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean hasFocus) {
        if (index == -1 && value == null) setText(title);
        else setText(value.toString());
        return this;
    }
}
