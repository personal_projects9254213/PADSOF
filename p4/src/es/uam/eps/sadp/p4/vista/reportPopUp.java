package es.uam.eps.sadp.p4.vista;

import es.uam.eps.sadp.p3.*;
import es.uam.eps.sadp.p4.vista.figuras.*;

import java.awt.*;
import java.awt.event.*;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.*;


public class reportPopUp extends JDialog {
	private JLabel label;
	public JButton accept;
	public JButton reject;
	public JButton play;

	public reportPopUp(es.uam.eps.sadp.p3.System app,Ticket t) {
		super.setResizable(false);
		super.setSize(400,225);
		super.setLocationRelativeTo(null);
		setModalityType(Dialog.DEFAULT_MODALITY_TYPE);

		setLayout(null);
		label= new JLabel("Ban");
		label.setBounds(40,40,300,40);
		label.setFont(new Font("", Font.PLAIN,  18));
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setVerticalAlignment(SwingConstants.CENTER);

		play = new RoundRectButton("Play");
		play.setBounds(160,90,80,20);

		accept = new RoundRectButton("Accept");
		accept.setBounds(100,120,80,20);

		reject = new RoundRectButton("Reject");
		reject.setBounds(220,120,80,20);
		
		play.addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						app.play(t.getSong());
					}
				}
				);

		accept.addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						app.stop();
						app.checkReport(t,true);
						dispose();
					}
				}
				);


		reject.addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent g) {
						app.stop();
						app.checkReport(t,false);
						dispose();
					}
				}
				);

		add(play);
		add(label);
		add(accept);
		add(reject);
		
		/*stops song when window is closed*/
		super.addWindowListener(new WindowAdapter(){
			@Override
			public void windowClosing(WindowEvent e)
			{
				app.stop();				
				e.getWindow().dispose();
			}
		});

		setVisible(true);
	}
}