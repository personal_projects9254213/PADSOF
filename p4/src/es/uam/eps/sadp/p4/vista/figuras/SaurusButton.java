package es.uam.eps.sadp.p4.vista.figuras;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Shape;
import java.awt.geom.RoundRectangle2D;

import javax.swing.JButton;

public class SaurusButton extends JButton {

	public SaurusButton(String text){
		super(text);
		Dimension size = getPreferredSize();
		size.width = size.height = Math.max(size.width, size.height);
		setPreferredSize(size);
		setBorder(null);
		setBackground(Color.BLACK);
		setFont(new Font("SansSerif", Font.BOLD+Font.ITALIC, 14));
		setForeground(Color.YELLOW);
	}
	
}
