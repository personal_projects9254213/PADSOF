package es.uam.eps.sadp.p4.vista;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.*;

import es.uam.eps.sadp.p3.Album;
import es.uam.eps.sadp.p3.Playlist;
import es.uam.eps.sadp.p3.Registered;
import es.uam.eps.sadp.p3.Song;
import es.uam.eps.sadp.p4.vista.figuras.PlaylistComboBox;
import es.uam.eps.sadp.p4.vista.figuras.RoundRectButton;


public class PlayZone extends JPanel  {

	private Gui gui;
    public PlaylistComboBox combo;
    private JLabel label1, label2;
    private JProgressBar bar;
    public JButton play, stop;
    public JButton playlist, report;
    private JLabel cover;
    private Image logoImage, playImage, stopImage;
    public Song s = null;
    public Album a = null;
    public Playlist actual = null;
    
    public PlayZone (Gui gui) {
    	this.gui = gui;
		setSize(325,400);
		setLayout(null);
		
				
        label1 = new JLabel();
        label1.setBounds(25, 10, 225, 20);
        label1.setFont(new Font("", Font.PLAIN,  14));
        label1.setHorizontalAlignment(SwingConstants.CENTER);
        add(label1);
        
        label2 = new JLabel();
        label2.setBounds(25, 30, 225, 20);
        label2.setFont(new Font("", Font.PLAIN,  14));
        label2.setHorizontalAlignment(SwingConstants.CENTER);
        add(label2);
        
        try {
			logoImage = ImageIO.read(new File("./images/noSong.gif"));
		} catch (IOException e) {
		}
		
        cover = new JLabel(new ImageIcon(logoImage.getScaledInstance(125, 125, Image.SCALE_SMOOTH)));
        cover.setBounds(75, 60, 125, 125);
		add(cover);
        
		bar = new JProgressBar(0,100);
	    bar.setStringPainted(false);
	    bar.setBounds(75, 200, 125, 20);
	    add(bar);
	    
		try {
			playImage = ImageIO.read(new File("./images/play.gif"));
		} catch (IOException e) {
		}
		
        play = new JButton();
        play.setIcon(new ImageIcon(playImage.getScaledInstance(40, 40, Image.SCALE_SMOOTH)));
        play.setBounds(120, 230, 40, 40);
        play.setContentAreaFilled(false);
        play.setBorder(null);
        add(play);
        play.setVisible(true);
        play.addActionListener(event->{
        	play.setVisible(false);
        	stop.setVisible(true);
        });
        
        try {
			stopImage = ImageIO.read(new File("./images/stop.gif"));
		} catch (IOException e) {
		}
		
        stop = new JButton();
        stop.setIcon(new ImageIcon(stopImage.getScaledInstance(35, 35, Image.SCALE_SMOOTH)));
        stop.setBounds(120, 230, 40, 40);
        stop.setContentAreaFilled(false);
        stop.setBorder(null);
        add(stop);
        stop.setVisible(false);
        stop.addActionListener(event->{
        	stop.setVisible(false);
        	play.setVisible(true);
        });
        
        combo = new PlaylistComboBox();
        combo.setLocation(15, 280);
        combo.setVisible(false);
        add(combo);
        
        playlist = new RoundRectButton("Quit from playlist");
        playlist.setBounds(15, 280, 140,20);
        playlist.setVisible(false);
        add(playlist);
        
        report = new RoundRectButton("Report");
        report.setBounds(170, 280, 80,20);
        report.setVisible(false);
        add(report);


    }
    public void setSong(es.uam.eps.sadp.p3.System system,Song song) {
    	Registered user = (Registered)system.currentUser();
    	List<String> ls = new ArrayList<String>();
    	ls.add("New Playlist");
    	for(Playlist p:user.getPlaylists()) {
    		ls.add(p.toString()); 
    	}
    	combo.setModel(new DefaultComboBoxModel(ls.toArray()));
    	if(song.getAlbum() != null) {
    		label1.setText(song.getTitle()+" - "+song.getAlbum().getTitle());
    	} else {
    		label1.setText(song.getTitle());
    	}
    	label2.setText(song.getAuthor().getNick());
    	if(s != null) {
    		if(!s.equals(song)) {
    			play.setVisible(true);
    			stop.setVisible(false);
    		}
    	}
    	s = song;
    }
    public void setSong(Song song) {
   
    	if(song.getAlbum() != null) {
    		label1.setText(song.getTitle()+" - "+song.getAlbum().getTitle());
    	} else {
    		label1.setText(song.getTitle());
    	}
    	label2.setText(song.getAuthor().getNick());
    	if(s != null) {
    		if(!s.equals(song)) {
    			play.setVisible(true);
    			stop.setVisible(false);
    		}
    	}
    	s = song;
    }


}
