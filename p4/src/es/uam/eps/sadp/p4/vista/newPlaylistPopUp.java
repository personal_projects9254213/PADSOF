package es.uam.eps.sadp.p4.vista;

import es.uam.eps.sadp.p3.Album;
import es.uam.eps.sadp.p3.Playlist;
import es.uam.eps.sadp.p3.Registered;
import es.uam.eps.sadp.p3.Song;
import es.uam.eps.sadp.p4.vista.figuras.*;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;


public class newPlaylistPopUp extends JDialog {
	private JLabel label;
	private JTextField campo;
	private JButton accept;
	private JButton reject;
	
	public newPlaylistPopUp(PlaylistComboBox playlistComboBox,PlaylistComboBox playlistComboBox2,PlaylistComboBox playlistComboBox3,Registered r,Song s) {
		super.setResizable(false);
		super.setSize(400,225);
		super.setLocationRelativeTo(null);
		setModalityType(Dialog.DEFAULT_MODALITY_TYPE);
		
		setLayout(null);
		label= new JLabel("Name of the new playlist:");
		label.setBounds(0,30,400,40);
		label.setFont(new Font("", Font.PLAIN,  18));
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setVerticalAlignment(SwingConstants.CENTER);
		
		campo = new JTextField(20);
		campo.setBounds(100,80,200,20);
		
		accept = new RoundRectButton("Accept");
		accept.setBounds(100,120,80,20);
		
		reject = new RoundRectButton("Cancel");
		reject.setBounds(220,120,80,20);
		
		
		accept.addActionListener(
			new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					playlistComboBox.addItem(campo.getText());
					playlistComboBox2.addItem(campo.getText());
					playlistComboBox3.addItem(campo.getText());
					new Playlist(campo.getText(),s,r);
					dispose();
				}
			}
		);
		
		
		reject.addActionListener(
			new ActionListener() {
				public void actionPerformed(ActionEvent g) {
					dispose();
				}
			}
		);
			
		
		add(label);
		add(campo);
		add(accept);
		add(reject);
		
		super.setVisible(true);
	}

	public newPlaylistPopUp(PlaylistComboBox playlistComboBox,PlaylistComboBox playlistComboBox2,PlaylistComboBox playlistComboBox3,Registered r,Playlist s) {
		super.setResizable(false);
		super.setSize(400,225);
		super.setLocationRelativeTo(null);
		setModalityType(Dialog.DEFAULT_MODALITY_TYPE);
		
		setLayout(null);
		label= new JLabel("Name of the new playlist:");
		label.setBounds(0,30,400,40);
		label.setFont(new Font("", Font.PLAIN,  18));
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setVerticalAlignment(SwingConstants.CENTER);
		
		campo = new JTextField(20);
		campo.setBounds(100,80,200,20);
		
		accept = new RoundRectButton("Accept");
		accept.setBounds(100,120,80,20);
		
		reject = new RoundRectButton("Cancel");
		reject.setBounds(220,120,80,20);
		
		
		accept.addActionListener(
			new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					playlistComboBox.addItem(campo.getText());
					playlistComboBox2.addItem(campo.getText());
					playlistComboBox3.addItem(campo.getText());
					new Playlist(campo.getText(),s,r);
					dispose();
				}
			}
		);
		
		
		
		reject.addActionListener(
			new ActionListener() {
				public void actionPerformed(ActionEvent g) {
					dispose();
				}
			}
		);
			
		
		add(label);
		add(campo);
		add(accept);
		add(reject);
		
		super.setVisible(true);
	}

	public newPlaylistPopUp(PlaylistComboBox playlistComboBox,PlaylistComboBox playlistComboBox2,PlaylistComboBox playlistComboBox3,Registered r,Album s) {
		super.setResizable(false);
		super.setSize(400,225);
		super.setLocationRelativeTo(null);
		setModalityType(Dialog.DEFAULT_MODALITY_TYPE);
		
		setLayout(null);
		label= new JLabel("Name of the new playlist:");
		label.setBounds(0,30,400,40);
		label.setFont(new Font("", Font.PLAIN,  18));
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setVerticalAlignment(SwingConstants.CENTER);
		
		campo = new JTextField(20);
		campo.setBounds(100,80,200,20);
		
		accept = new RoundRectButton("Accept");
		accept.setBounds(100,120,80,20);
		
		reject = new RoundRectButton("Cancel");
		reject.setBounds(220,120,80,20);
		
		
		accept.addActionListener(
			new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					playlistComboBox.addItem(campo.getText());
					playlistComboBox2.addItem(campo.getText());
					playlistComboBox3.addItem(campo.getText());
					new Playlist(campo.getText(),s,r);
					dispose();
				}
			}
		);
		
		
		reject.addActionListener(
			new ActionListener() {
				public void actionPerformed(ActionEvent g) {
					dispose();
				}
			}
		);
			
		
		add(label);
		add(campo);
		add(accept);
		add(reject);
		
		super.setVisible(true);
	}
}