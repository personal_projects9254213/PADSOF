package es.uam.eps.sadp.p4.vista;

import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.*;

import es.uam.eps.sadp.p4.vista.figuras.MenuButton;

public class AdminMenuBar extends JPanel {
	private Gui gui;
	public JButton disconnect;
	private JLabel logo;
	private Image logoImage;
	
	public AdminMenuBar(Gui gui) {
		this.gui = gui;
		setSize(175,525);
		setLayout(null);
		
		
		try {
			logoImage = ImageIO.read(new File("./images/logo.gif"));
		} catch (IOException e) {
		}
		
		logo = new JLabel(new ImageIcon(logoImage.getScaledInstance(175, 175, Image.SCALE_SMOOTH)));
		logo.setBounds(0, 25, 175, 175);
		add(logo);

		disconnect = new MenuButton("Disconnect");
		disconnect.setBounds(40, 210, 135, 30);
		add(disconnect);
		
	}
}