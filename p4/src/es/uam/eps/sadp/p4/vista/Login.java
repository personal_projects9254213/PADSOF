package es.uam.eps.sadp.p4.vista;

import javax.swing.*;

import es.uam.eps.sadp.p4.vista.figuras.RoundRectButton;

import java.awt.*;

public class Login extends JPanel {
	private Gui gui;
	private JLabel label, username, password;
	public JTextField usernameRead;
	public JPasswordField passwordRead;
	public JButton login, signin;
	
	public Login(Gui gui) {
		this.gui = gui;
		setSize(725,400);
		setLayout(null);

		label = new JLabel ("Dino's Music");
		label.setBounds(150,40, 300, 40);
		label.setFont(new Font("", Font.PLAIN,  32));
		label.setHorizontalAlignment(SwingConstants.CENTER);
		this.add(label);
		
		username = new JLabel("Username");
		username.setBounds(150,100, 80, 20);
		this.add(username);
		
		usernameRead = new JTextField(200);
		usernameRead.setBounds(230, 100, 200, 20);
		usernameRead.setBorder(BorderFactory.createLineBorder(Color.lightGray, 1));
		this.add(usernameRead);

		password = new JLabel("Password");
		password.setBounds(150, 140, 80, 20);
		this.add(password);
		
		passwordRead = new JPasswordField(200);
		passwordRead.setBounds(230, 140, 200, 20);
		passwordRead.setBorder(BorderFactory.createLineBorder(Color.lightGray, 1));
		this.add(passwordRead);
		
		login = new RoundRectButton("Log in");
		login.setBounds(200,180,80,20);
		this.add(login);
		
		signin = new RoundRectButton("Sign in");
		signin.setBounds(320,180,80,20);
		this.add(signin);
		signin.addActionListener(event -> {
			gui.setAllNotVisible();
			gui.unregisteredMenuBar.setVisible(true);
			gui.search.setVisible(true);
			gui.signin.setVisible(true);
		});
		
	}
}

