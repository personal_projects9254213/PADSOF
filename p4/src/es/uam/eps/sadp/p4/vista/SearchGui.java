package es.uam.eps.sadp.p4.vista;

import es.uam.eps.sadp.p4.vista.figuras.*;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;


public class SearchGui extends JPanel implements ActionListener {
	public JTextField field;
	public JButton search;
	public JComboBox filter;
	private Gui gui;
	
	public SearchGui(Gui gui) {
		
		
		this.gui = gui;
		
		setSize(440,20);
		setLayout(null);
		
		field = new JTextField(20);
		field.setBounds(0,0,290,20);
		field.setBorder(BorderFactory.createLineBorder(Color.lightGray, 1));
		
		filter = new SearchComboBox();
		filter.setLocation(290,0);
		
		search = new RoundRectButton("Search");
		search.setBounds(360,0,80,20);
		
		this.add(field);
		this.add(search);
		this.add(filter);
		
	}
	

	@Override
	public void actionPerformed(ActionEvent e) {
	}
}
//}