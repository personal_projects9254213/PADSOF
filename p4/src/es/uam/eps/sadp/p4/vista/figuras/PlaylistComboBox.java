package es.uam.eps.sadp.p4.vista.figuras;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComboBox;

import es.uam.eps.sadp.p3.System;
import es.uam.eps.sadp.p4.vista.newPlaylistPopUp;

public class PlaylistComboBox extends JComboBox<String>{
	
	public PlaylistComboBox() {
		super();
		addItem("New Playlist");
		setBounds(0,0,140,20);
		setRenderer(new MyComboBoxRenderer("Add to Playlist"));
		setSelectedIndex(-1);
		
	}

}
