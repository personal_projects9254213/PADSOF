package es.uam.eps.sadp.p4.vista;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import es.uam.eps.sadp.p4.vista.figuras.RoundRectButton;
import es.uam.eps.sadp.p4.vista.figuras.SaurusButton;

public class BecomeSaurus extends JPanel {
	public JButton premium;
	private Gui gui;
	
	public BecomeSaurus(Gui gui) {
		this.gui = gui;
		
		setSize(150,20);
		setLayout(null);
		
		premium = new SaurusButton("Become a Saurus");
		premium.setBounds(0,0,150,20);
		
		this.add(premium);
	}
	
}