package es.uam.eps.sadp.p4.vista;
import java.util.*;
import java.util.List;

import javax.swing.*;

import es.uam.eps.sadp.p3.Album;
import es.uam.eps.sadp.p3.Playlist;
import es.uam.eps.sadp.p3.Registered;
import es.uam.eps.sadp.p3.Song;
import es.uam.eps.sadp.p4.vista.figuras.PlaylistComboBox;

import java.awt.*;
import java.awt.event.*;
import java.time.LocalDate;

	public class SearchedList extends JPanel{
		private Gui gui;
		public JList list;
		private JLabel nombre;
		private MouseListener mouseListener;
		public String type;
		public PlaylistComboBox comboBox;
		
	    public SearchedList(Gui gui) {
		    this.gui = gui;
			setSize(400,400);
			setLayout(null);
			
			nombre= new JLabel();	
			nombre.setBounds(20,0,220,30);	
			nombre.setFont(new Font("", Font.PLAIN,  22));	

	 		this.add(nombre);
	 		
			list = new JList();
			list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			list.setLayoutOrientation(JList.VERTICAL);
			list.setSelectedIndex(-1);
			list.setBorder(BorderFactory.createLineBorder(Color.lightGray, 2));
			JScrollPane listView = new JScrollPane(list);
			listView.setBounds(20, 40, 380, 340);
			this.add(listView);
			comboBox = new PlaylistComboBox();
			comboBox.setLocation(260, 5);
			this.add(comboBox);
			comboBox.setVisible(false);
			mouseListener = new MouseAdapter() {
				public void mouseClicked(MouseEvent e) {
				
				}
			};
			list.addMouseListener(mouseListener);
	    }
	    public void setList(es.uam.eps.sadp.p3.System system,List<? extends Object> elements,String tp) {
	    	Registered user = (Registered)system.currentUser();
	    	List<String> ls = new ArrayList<String>();
	    	
	    	nombre.setVisible(false);
	    	ls.add("New Playlist");
	    	for(Playlist p:user.getPlaylists()) {
	    		ls.add(p.toString()); 
	    	}
	    	comboBox.setModel(new DefaultComboBoxModel(ls.toArray()));
	    	type = tp;
	    	Object[] array = elements.toArray();
	    	list.setListData(array);
	    }
	    
	    public void setList(es.uam.eps.sadp.p3.System system,List<? extends Object> elements,String tp, String name) {
	    	setList(system, elements, tp);
	    	nombre.setText(name);
	    	nombre.setVisible(true);
	    }
	    public void setList(List<? extends Object> elements,String tp) {
	    	nombre.setVisible(false);
	    	type = tp;
	    	Object[] array = elements.toArray();
	    	list.setListData(array);
	    }
	    public void setList(List<? extends Object> elements,String tp, String name) {
	    	setList(elements,tp);
	    	nombre.setText(name);
	    	nombre.setVisible(true);
	    }
	}
	
