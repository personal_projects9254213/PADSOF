package es.uam.eps.sadp.p4.vista;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.*;

import java.util.ArrayList;
import java.util.List;

import javax.swing.*;
import javax.swing.event.*;


import es.uam.eps.sadp.p4.vista.figuras.RoundRectButton;
import es.uam.eps.sadp.p4.vista.figuras.SaurusButton;
import es.uam.eps.sadp.p3.*;
import es.uam.eps.sadp.p3.Ticket.TicketType;

public class Notifications extends JPanel {
	private JLabel label;
	private Gui gui;
	public JList list;
	public DefaultListModel listModel;
	List<Ticket> ticket = new ArrayList<>();
	
	
	public Notifications(Gui gui) {
		this.gui = gui;
		setSize(400,400);
		setLayout(null);

		
		label= new JLabel("Notifications");
		label.setBounds(20,0,220,30);
		label.setFont(new Font("", Font.PLAIN,  22));

		this.add(label);
		Song cancion = null;
		for(int i = 0; i < 10; i++) {
			ticket.add(new Ticket(TicketType.NOTIFICATION, cancion));
		}
		listModel = new DefaultListModel();
		list = new JList(listModel);
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.setLayoutOrientation(JList.VERTICAL);
		list.setSelectedIndex(-1);
		list.setBorder(BorderFactory.createLineBorder(Color.lightGray, 2));
		JScrollPane listView = new JScrollPane(list);
		listView.setBounds(20, 40, 380, 340);
		this.add(listView);
		
	}
	public void addNotification(List<Ticket> t) {
		listModel.clear();
		for(Ticket currentTicket : t) {
			listModel.addElement(currentTicket);
		}
	}
}
