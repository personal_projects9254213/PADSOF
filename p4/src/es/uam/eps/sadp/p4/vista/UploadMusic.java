package es.uam.eps.sadp.p4.vista;

import javax.swing.*;

import es.uam.eps.sadp.p4.vista.figuras.RoundRectButton;

import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.util.*;

public class UploadMusic extends JPanel {
	private Gui gui;
	private JLabel label;
	public JButton upload;
	public JTextArea log;
	public JFileChooser fc;
	
	
	public UploadMusic(Gui gui) {
		this.gui = gui;
		setSize(400,400);
		setLayout(null);
		
		label = new JLabel ("Upload Music");
	    label.setBounds(20,0,220,30);
		label.setFont(new Font("", Font.PLAIN,  22));
		this.add(label);
		
		upload =  new RoundRectButton("Upload");
		upload.setBounds(320,160,80,20);
		this.add(upload);

		
		log = new JTextArea(20,10);
	    log.setEditable(false);
	    log.setBounds(40,160,280,20);
	    log.setFont(new Font("", Font.PLAIN,  16));
	    log.setBorder(BorderFactory.createLineBorder(Color.lightGray, 1));
	    this.add(log);
	 
	    //Create a file chooser
	    fc = new JFileChooser();
	    fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
		
	}
}
