package es.uam.eps.sadp.p4.vista.figuras;

import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

public class Imagen extends JPanel {
 
private String str;

public Imagen(String str) {
this.str = str;
this.setSize(175, 100); 
}
 
 
public void paint(Graphics grafico) {

	@SuppressWarnings("unused")
	Dimension height = getSize();
	ImageIcon Img = new ImageIcon(getClass().getResource(str)); 
	
	
	grafico.drawImage(Img.getImage(), 35 ,15, 120, 100, null);
	
	setOpaque(false);
		super.paintComponent(grafico);
	}
}