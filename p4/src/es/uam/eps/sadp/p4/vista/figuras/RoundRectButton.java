package es.uam.eps.sadp.p4.vista.figuras;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Shape;
import java.awt.geom.RoundRectangle2D;

import javax.swing.JButton;

public class RoundRectButton extends JButton {

	public RoundRectButton(String text){
		super(text);
		Dimension size = getPreferredSize();
		size.width = size.height = Math.max(size.width, size.height);
		setPreferredSize(size);
		setFont(new Font("SansSerif", Font.BOLD, 11));
	}
	
	protected void paintComponent(Graphics g) {
	     if (getModel().isArmed()) {
	          g.setColor(Color.lightGray);
	     } else {
	          g.setColor(getBackground());
	     }
	     g.fillRoundRect(0, 0, getWidth()-1, getHeight()-1, 10, 10);
	     super.paintComponent(g);
	}
	protected void paintBorder(Graphics g) {
	     g.setColor(getForeground());
	     g.drawRoundRect(0, 0, getWidth()-1, getHeight()-1, 10, 10);
	}
	Shape shape;
	public boolean contains(int x, int y) {
	     if (shape == null || !shape.getBounds().equals(getBounds())) {
	          shape = new RoundRectangle2D.Float(0, 0, getWidth()-1, getHeight()-1, 10, 10);
	     }
	     return shape.contains(x, y);
	}
}
