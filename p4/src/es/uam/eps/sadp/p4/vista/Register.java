package es.uam.eps.sadp.p4.vista;

import javax.swing.*;
import java.awt.*;
import java.util.*;

import javax.swing.JButton;
import javax.swing.JLabel;

import es.uam.eps.sadp.p4.vista.figuras.DateLabelFormatter;
import es.uam.eps.sadp.p4.vista.figuras.RoundRectButton;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;
import java.util.Date;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import net.sourceforge.jdatepicker.impl.JDatePanelImpl;
import net.sourceforge.jdatepicker.impl.JDatePickerImpl;
import net.sourceforge.jdatepicker.impl.SqlDateModel;
import net.sourceforge.jdatepicker.impl.UtilCalendarModel;
import net.sourceforge.jdatepicker.impl.UtilDateModel;

public class Register extends JPanel {
	private Gui gui;
	private JLabel label, username, password, name, birthDate;
	public JTextField usernameRead, nameRead;
	public JPasswordField passwordRead;
	public JButton login, signin;
	public JDatePickerImpl datePicker;
	
	public Register(Gui gui) {
		this.gui = gui;
		setSize(725,400);
		setLayout(null);

		label = new JLabel ("Dino's Music");
		label.setBounds(200,40, 200, 40);
		label.setFont(new Font("", Font.PLAIN,  32));
		this.add(label);
		
		name = new JLabel("Name");
		name.setBounds(150,100, 80, 20);
		this.add(name);
		
		nameRead = new JTextField(200);
		nameRead.setBounds(230, 100, 200, 20);
		nameRead.setBorder(BorderFactory.createLineBorder(Color.lightGray, 1));
		this.add(nameRead);
		
		username = new JLabel("Nick");
		username.setBounds(150,140, 80, 20);
		this.add(username);
		
		usernameRead = new JTextField(200);
		usernameRead.setBounds(230, 140, 200, 20);
		usernameRead.setBorder(BorderFactory.createLineBorder(Color.lightGray, 1));
		this.add(usernameRead);

		password = new JLabel("Password");
		password.setBounds(150, 180, 80, 20);
		this.add(password);
		
		passwordRead = new JPasswordField(200);
		passwordRead.setBounds(230, 180, 200, 20);
		passwordRead.setBorder(BorderFactory.createLineBorder(Color.lightGray, 1));
		this.add(passwordRead);
		
		birthDate = new JLabel("Birth Date");
		birthDate.setBounds(150, 220, 80, 20);
		this.add(birthDate);
		
		UtilDateModel model = new UtilDateModel();
		model.setDate(1990, 8, 24);
		model.setSelected(true);
		
		JDatePanelImpl datePanel = new JDatePanelImpl(model);
		
		datePicker = new JDatePickerImpl(datePanel, new DateLabelFormatter());
		
		datePicker.getComponent(0).setFont(new Font("", Font.BOLD, 12));
		datePicker.getComponent(0).setBackground(Color.white);
		((JFormattedTextField) datePicker.getComponent(0)).setBorder(BorderFactory.createLineBorder(Color.lightGray, 1));
		datePicker.getComponent(0).setPreferredSize(new Dimension(160,20)); 
	    datePicker.getComponent(1).setPreferredSize(new Dimension(39,20));
	    ((JButton) datePicker.getComponent(1)).setBorder(BorderFactory.createLineBorder(Color.lightGray, 1));
		datePicker.setBounds(230, 220, 200, 20);
		this.add(datePicker);
		
		login = new RoundRectButton("Log in");
		login.setBounds(200,260,80,20);
		this.add(login);
		login.addActionListener(event -> {
			gui.setAllNotVisible();
			gui.unregisteredMenuBar.setVisible(true);
			gui.search.setVisible(true);
			gui.login.setVisible(true);
		});
		
		signin = new RoundRectButton("Sign in");
		signin.setBounds(320,260,80,20);
		this.add(signin);
		
	}
}


