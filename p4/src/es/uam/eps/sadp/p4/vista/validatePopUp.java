package es.uam.eps.sadp.p4.vista;

import es.uam.eps.sadp.p3.Ticket;
import es.uam.eps.sadp.p3.System;
import es.uam.eps.sadp.p4.vista.figuras.*;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;


public class validatePopUp extends JDialog {
	private JLabel label;
	private JButton accept;
	private JButton explicit;
	private JButton reject;
	public JButton play;

	public validatePopUp(es.uam.eps.sadp.p3.System app,Ticket t) {
		super.setResizable(false);
		super.setSize(400,225);
		super.setLocationRelativeTo(null);
		setModalityType(Dialog.DEFAULT_MODALITY_TYPE);

		setLayout(null);
		label= new JLabel("Validate");
		label.setBounds(40,40,300,40);
		label.setFont(new Font("", Font.PLAIN,  18));
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setVerticalAlignment(SwingConstants.CENTER);

		play = new RoundRectButton("Play");
		play.setBounds(160,90,80,20);
		if(t.getSong() == null) {
			play.setVisible(false);
		}

		accept = new RoundRectButton("Accept");
		accept.setBounds(50,120,90,20);

		explicit = new RoundRectButton("Explicit");
		explicit.setBounds(155,120,90,20);

		reject = new RoundRectButton("Reject");
		reject.setBounds(260,120,90,20);
		
		play.addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						app.play(t.getSong());
					}
				}
				);

		accept.addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						app.stop();
						app.validate(t, true, false);
						dispose();
					}
				}
				);

		explicit.addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent f) {
						app.stop();
						app.validate(t, true, true);
						dispose();
					}
				}
				);

		reject.addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent g) {
						app.stop();
						app.validate(t, false, false);
						dispose();
					}
				}
				);

		add(label);
		add(play);
		add(accept);
		add(explicit);
		add(reject);
		
		/*stops song when window is closed*/
		super.addWindowListener(new WindowAdapter(){
			@Override
			public void windowClosing(WindowEvent e)
			{
				app.stop();				
				e.getWindow().dispose();
			}
		});

		super.setVisible(true);
	}
}