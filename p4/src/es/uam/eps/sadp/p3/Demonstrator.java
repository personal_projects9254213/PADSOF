package es.uam.eps.sadp.p3;

import java.time.LocalDate;
import java.util.*;

public class Demonstrator{
	public static void main(String[] args) {
	List<Song> ls;
	List<Ticket> lt;
	List<Album> la;
	List<Registered> lu;
	LocalDate birthDate = LocalDate.of(1999,6,28);
	LocalDate birthDateUnder = LocalDate.of(2012,6,28);
	Map<String, String> albumSongs = Map.of("./testSong/song3.mp3","first one","./testSong/song4.mp3","second one");
	
	//creation of the system
	System system = System.getInstance();
	
	//users 1 and 3 register and the user 3 follows the user 1
	system.register("name1","nick1","password1",birthDate);
	system.register("name3","nick3","password3",birthDate);
	system.register("name4","nick4","password4",birthDate);
	system.login("nick3", "password3");
	lu = system.searchProfile("nick1");
	system.follow(lu.get(0));
	system.disconnect();
	
	//the user 1 uploads new songs and an album
	system.login("nick1", "password1");
	system.upload("./testSong/song2.mp3", "ok soud");
	system.upload("./testSong/song1.mp3", "fail soud");
	system.upload("the Album", albumSongs);
	system.disconnect();
	
	//administrator validates the songs and listens to them
	system.login("Rex", "SuperSecret");
	lt = system.getTickets();
	//the first one is marked as not explicit
	system.validate(lt.get(0), true, false);
	//the second one is marked as explicit
	system.validate(lt.get(0), true, true);
	//the third one is the album(not explicit)
	system.validate(lt.get(0), true, false);
	ls = system.searchSong("ok");
	if(!system.play(ls.get(0))) {
		java.lang.System.out.println("Error : 1");
		System.deleteSaveFile();
		return;
	}
	system.disconnect();
	
	//the user who follows him gets notified
	system.login("nick3", "password3");
	lt = system.getTickets();
	if(lt.size() != 3) {
		java.lang.System.out.println("Error : 2");
		System.deleteSaveFile();
		return;
	}
	system.disconnect();
	
	//a not overage user tries to listen to both songs
	system.register("name2","nick2","password2",birthDateUnder);
	system.login("nick2", "password2");
	ls = system.searchSong("ok");
	if(!system.play(ls.get(0))) {
		java.lang.System.out.println("Error : 3");
		System.deleteSaveFile();
		return;
	}
	//he fails his try to find the explicit one and so does the unregistered
	ls = system.searchSong("fail");
	if(ls.size() > 0) {
		java.lang.System.out.println("Error : 4");
		System.deleteSaveFile();
		return;
	}
	system.disconnect();
	ls = system.searchSong("fail");
	if(ls.size() > 0) {
		java.lang.System.out.println("Error : 4");
		System.deleteSaveFile();
		return;
	}
	
	//a user tries to listen both songs in the album
	system.login("nick2", "password2");
	la = system.searchAlbum("the Album");
	if(!system.play(la.get(0).getSongs().get(0))) {
		java.lang.System.out.println("Error : 5");
		System.deleteSaveFile();
		return;
	}
	if(!system.play(la.get(0).getSongs().get(1))) {
		java.lang.System.out.println("Error : 6");
		System.deleteSaveFile();
		return;
	}
	
	//a user listens to some music of an author
	lu = system.searchProfile("nick1");
	ls = lu.get(0).getSongs();
	if(!system.play(ls.get(0))) {
		java.lang.System.out.println("Error : 7");
		System.deleteSaveFile();
		return;
	}
	if(system.play(ls.get(1))) {
		java.lang.System.out.println("Error : 8");
		System.deleteSaveFile();
		return;
	}
	if(!system.play(ls.get(2))) {
		java.lang.System.out.println("Error : 9");
		System.deleteSaveFile();
		return;
	}
	
	//a user reports a song
	if(!system.report(ls.get(0))) {
		java.lang.System.out.println("Error : 10");
		System.deleteSaveFile();
		return;
	}
	system.disconnect();
	
	//the administrator confirms the report as correct
	system.login("Rex", "SuperSecret");
	lt = system.getTickets();
	if(!system.checkReport(lt.get(0), true)) {
		java.lang.System.out.println("Error : 11");
		System.deleteSaveFile();
		return;
	}
	//as consequence the song is erased and the user who uploaded it banned
	ls = system.searchSong("ok");
	if(!ls.isEmpty()){
		java.lang.System.out.println("Error : 12");
		System.deleteSaveFile();
		return;
	}
	system.disconnect();
	if(system.login("nick1", "password1")) {
		java.lang.System.out.println("Error : 13");
		System.deleteSaveFile();
		return;
	}
	
	//a user reports a song
	system.login("nick3", "password3");
	ls = system.searchSong("fail");
	system.report(ls.get(0));
	system.disconnect();
	
	//the administrator confirms the report as incorrect
	system.login("Rex", "SuperSecret");
	lt = system.getTickets();
	system.checkReport(lt.get(0), false);
	system.disconnect();
	//the user who reported gets banned for a month
	if(system.login("nick3", "password3")) {
		java.lang.System.out.println("Error : 14");
		System.deleteSaveFile();
		return;
	}
	
	//a user uploads a song
	system.login("nick2", "password2");
	system.upload("./testSong/song2.mp3", "ok soudfake");
	system.disconnect();
	
	//the administrator marks it as non valid
	system.login("Rex", "SuperSecret");
	lt = system.getTickets();
	//the first one is marked as not explicit
	system.validate(lt.get(0), false, false);
	system.disconnect();
	//The user has three days to reupload the song for a new check
	//in that time the sound is not valid
	ls = system.searchSong("ok soudfake");
	if(!ls.isEmpty()){
		java.lang.System.out.println("Error : 15");
		System.deleteSaveFile();
		return;
	}
	
	//after the reupload if it gets validated it works
	system.login("nick2", "password2");
	lu = system.searchProfile("nick2");
	ls = lu.get(0).getSongs();
	ls.get(0).setTitle("not offensive");
	system.reupload(ls.get(0));
	system.disconnect();
	//this time the administrator validates it
	system.login("Rex", "SuperSecret");
	lt = system.getTickets();
	system.validate(lt.get(0), true, false);
	system.disconnect();
	ls = system.searchSong("not offensive");
	if(ls.isEmpty()){
		java.lang.System.out.println("Error : 16");
		System.deleteSaveFile();
		return;
	}
	
	//become premium by paying
	system.login("nick2", "password2");
	system.becomePremium("1234567890123456");
	lu = system.searchProfile("nick2");
	lu.get(0).setListenedSongs(Unregistered.MAXSONGS);
	if(!lu.get(0).getPremium()) {
		java.lang.System.out.println("Error : 17");
		System.deleteSaveFile();
		return;
	}
	if(!system.play(ls.get(0))) {
		java.lang.System.out.println("Error : 18");
		System.deleteSaveFile();
		return;
	}
	
	
	//becoming premium for the views of a Song
	system.login("nick4", "password4");
	system.upload("./testSong/song5.mp3", "Dino's rock!");
	system.disconnect();
	//it is validated
	system.login("Rex", "SuperSecret");
	lt = system.getTickets();
	system.validate(lt.get(0), true, false);
	system.disconnect();
	//it gets as much reproductions as the milestone(in this case 1000)
	ls = system.searchSong("Dino's rock!");
	ls.get(0).setReproductions(1000);
	//now he can become premium for free
	system.login("nick4", "password4");
	system.becomePremium();
	lu = system.searchProfile("nick4");
	lu.get(0).setListenedSongs(Unregistered.MAXSONGS);
	if(!lu.get(0).getPremium()) {
		java.lang.System.out.println("Error : 19");
		System.deleteSaveFile();
		return;
	}
	if(!system.play(ls.get(0))) {
		java.lang.System.out.println("Error : 20");
		System.deleteSaveFile();
		return;
	}
	if(ls.get(0).getReproductions() != 1001){
		java.lang.System.out.println("Error : 21");
		System.deleteSaveFile();
		return;
	}
	
	java.lang.System.out.println("Everything functional, no errors in the demonstrator\n");
	System.deleteSaveFile();
	}	
}
