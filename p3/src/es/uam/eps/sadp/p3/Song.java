package es.uam.eps.sadp.p3;

import java.io.Serializable;
import java.time.*;

/**
 * Class Song
 * @author <a href="mailto:nicolas.serranos@estudiante.uam.es">Nicolas Serrano</a>
 * @author <a href="mailto:miguela.luque@estudiante.uam.es">Miguel Angel Luque</a>
 * @author <a href="mailto:blanca.mercado@estudiante.uam.es">Blanca Mercado</a>
 * Practice group: 2291
 */
public class Song implements Serializable {
	private static final long serialVersionUID = 1L;
	private String path;
    private String title;
    private Registered author;
	private Album album;
    private long reproductions;
    private boolean explicit;
    private ValidationType validate;
	private LocalDate rejectDate;
	private LocalDate nextResetDate;

	/**
	 * Enumeration TicketType defines the types of validation states
	 */
	public enum ValidationType {
		FALSE,
	    TRUE,
	    INPROGRESS;
	}

	/**
     * Constructor of the class song
	 * @param path path of the song
     * @param title title of the song
     * @param author author of the song
     */
	public Song(String path, String title, Registered author) {
		this(path, title, author, null);
	}

    /**
     * Constructor of the class song
	 * @param path path of the song
     * @param title title of the song
     * @param author author of the song
	 * @param album album of the song
     */
    public Song(String path, String title, Registered author, Album album) {
		this.path = path;
        this.title = title;
        this.author = author;
		this.album = album;
        reproductions = 0;
        validate = ValidationType.INPROGRESS;
		nextResetDate = LocalDate.now().plusMonths(1).withDayOfMonth(1);
    }

    public String getTitle() {
        return title;
    }

    public Registered getAuthor() {
        return author;
    }

    public String getPath() {
        return path;
    }

	/**
	 * Gets the reproductions of that month
	 * @return the number of reproductions
	 */
    public long getReproductions() {
		if(!nextResetDate.isAfter(LocalDate.now())){
            reproductions = 0;
            nextResetDate = nextResetDate.plusMonths(1);
        }
        return reproductions;
    }

    public boolean isExplicit() {
        return explicit;
    }

    public boolean isValidate() {
    	if(validate == ValidationType.TRUE) {
    		return true;
    	}
    	return false;
    }

    public ValidationType getValidate() {
    	return validate;
    }

	public Album getAlbum() {
		return album;
	}

	public LocalDate getRejectDate() {
		return rejectDate;
	}

	/**
     * Modifies the number of reproductions
     * @param reproductions to set
     */
    public void setReproductions(long reproductions) {
        this.reproductions = reproductions;
    }

    /**
     * Modifies the title
     * @param title: new title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Modifies the path
     * @param path: new path
     */
    public void setPath(String path) {
        this.path = path;
    }

    public void setValidate(ValidationType validation) {
        validate = validation;
    }

    /**
     * Validate a song
     * @param valid boolean if a song is valid or not
     * @param explicit boolean if a song is explicit or not
	 * @param rejectDate date when they are validated
     */
    public void validate(Boolean valid, Boolean explicit, LocalDate rejectDate) {
    	if(valid) {
    		validate = ValidationType.TRUE;

    	} else {
    		validate = ValidationType.FALSE;
    	}
    	this.explicit = explicit;
		this.rejectDate = rejectDate;
    }

    @Override
    public String toString(){
		if(album == null) {
			return title+" "+author.getNick();
		}
		return title+" "+album.getTitle()+" "+author.getNick();
	}

}
