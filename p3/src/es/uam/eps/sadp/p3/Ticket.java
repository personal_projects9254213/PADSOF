package es.uam.eps.sadp.p3;

import java.io.Serializable;

/**
 * Class Ticket
 * @author <a href="mailto:nicolas.serranos@estudiante.uam.es">Nicolas Serrano</a>
 * @author <a href="mailto:miguela.luque@estudiante.uam.es">Miguel Angel Luque</a>
 * @author <a href="mailto:blanca.mercado@estudiante.uam.es">Blanca Mercado</a>
 * Practice group: 2291
 */
public class Ticket implements Serializable {
	private static final long serialVersionUID = 1L;
	private TicketType type;
	private Registered user;
	private Song song;
	private Album album;

	/**
	 * Enumeration TicketType defines the types of ticket states
	 */
	public enum TicketType {
		REPORT {
			@Override
			public String toString() {
				return "REPORT";
			}
		},
		VALIDATION {
			@Override
			public String toString() {
				return "VALIDATION";
			}
		},
		NOTIFICATION {
			@Override
			public String toString() {
				return "NOTIFICATION";
			}
		};
	}

	/**
	 * Constructor of the class Ticket
	 * @param type type of ticket
	 * @param song song described in ticket
	 */
	public Ticket(TicketType type, Song song) {
		this.type = type;
		this.song = song;
	}

	/**
	 * Constructor of the class Ticket
	 * @param type type of ticket
	 * @param album album described in ticket
	 */
	public Ticket(TicketType type, Album album) {
		this.type = type;
		this.album = album;
	}

	/**
	 * Constructor of the class Ticket
	 * @param type type of ticket
	 * @param song song described in ticket
	 * @param user user creating the ticket
	 */
	public Ticket(TicketType type, Song song, Registered user) {
		this.type = type;
		this.song = song;
		this.user = user;
	}

	public TicketType getType() {
		return type;
	}
	public Song getSong() {
		return song;
	}
	public Album getAlbum() {
		return album;
	}
	public Registered getUser() {
		return user;
	}

	@Override
	public String toString(){
		return type.toString();
	}
}
