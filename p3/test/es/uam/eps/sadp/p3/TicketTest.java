/**
 * 
 */
package es.uam.eps.sadp.p3;
/**
 * Class ticket test
 * @author <a href="mailto:nicolas.serranos@estudiante.uam.es">Nicolas Serrano</a>
 * @author <a href="mailto:miguela.luque@estudiante.uam.es">Miguel Angel Luque</a>
 * @author <a href="mailto:blanca.mercado@estudiante.uam.es">Blanca Mercado</a>
 * Practice group: 2291
 */
import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;


public class TicketTest {
	
	 private Ticket t1;
	 private Ticket t2;
	 private Ticket t3;
	 private Registered user;
	 private Registered author;
	 private Song s1;
	 private Album a1;
	
	@Before
	public void setUp() throws Exception {
		user = new Registered("name1", "nick1", "password1", LocalDate.of(1999,6,28));
		author = new Registered("name1", "nick1", "password1", LocalDate.of(1999,6,28));
		a1 = new Album("a1", author);
		s1 = new Song("song1.mp3", "song1", author);
		t1 = new Ticket(Ticket.TicketType.REPORT, s1,user);
		t2 = new Ticket(Ticket.TicketType.VALIDATION, a1);
		t3 = new Ticket(Ticket.TicketType.NOTIFICATION, s1);
	}

	/**
	 * Test method for {@link es.uam.eps.sadp.p3.Ticket#getType()}.
	 */
	@Test
	public void testGetType() {
		assertEquals(t1.getType(),Ticket.TicketType.REPORT);
		assertEquals(t2.getType(),Ticket.TicketType.VALIDATION);
		assertEquals(t3.getType(),Ticket.TicketType.NOTIFICATION);
	}

	/**
	 * Test method for {@link es.uam.eps.sadp.p3.Ticket#getSong()}.
	 */
	@Test
	public void testGetSong() {
		assertEquals(t1.getSong(),s1);

	}

	/**
	 * Test method for {@link es.uam.eps.sadp.p3.Ticket#getAlbum()}.
	 */
	@Test
	public void testGetAlbum() {
		assertEquals(t2.getAlbum(),a1);
	}

	/**
	 * Test method for {@link es.uam.eps.sadp.p3.Ticket#getUser()}.
	 */
	@Test
	public void testGetUser() {
		assertEquals(t1.getUser(),user);
	}

}
