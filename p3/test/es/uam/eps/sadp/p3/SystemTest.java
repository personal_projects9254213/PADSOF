package es.uam.eps.sadp.p3;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.util.*;
import java.lang.reflect.Field;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The Class SystemTest.
 */
public class SystemTest {
	System app;

	/**
	 * Sets the up.
	 *
	 * @throws NoSuchFieldException the no such field exception
	 * @throws IllegalAccessException the illegal access exception
	 */
	@Before
	public void setUp() throws NoSuchFieldException, IllegalAccessException {
		Field instance = System.class.getDeclaredField("app");
	    instance.setAccessible(true);
	    instance.set(null, null);
		app = System.getInstance();
	}

	/**
	 * Test get instance.
	 */
	@Test
	public void testGetInstance() {
		assertSame(app, System.getInstance());
	}
	
	/**
	 * Test get tickets.
	 */
	@Test
	public void testGetTickets() {
		assertNull(app.getTickets());
		app.login("Rex", "SuperSecret");
		assertNotNull(app.getTickets());
		app.register("Rugrats Dino", "Reptar", "Tommy Pickles", LocalDate.of(1991, 8, 11));	
		app.login("Reptar", "Tommy Pickles");
		assertNotNull(app.getTickets());
	}
	
	/**
	 * Test disconnect.
	 */
	@Test
	public void testDisconnect() {
		assertTrue(app.disconnect());
	}

	/**
	 * Test register.
	 */
	@Test
	public void testRegister() {
		assertTrue(app.register("Rugrats Dino", "Reptar", "Tommy Pickles", LocalDate.of(1991, 8, 11)));
		assertFalse(app.register(null, "Reptar", "Tommy Pickles", LocalDate.of(1991, 8, 11)));
		assertFalse(app.register("Rugrats Dino", null, "Tommy Pickles", LocalDate.of(1991, 8, 11)));
		assertFalse(app.register("Rugrats Dino", "Reptar", null, LocalDate.of(1991, 8, 11)));
		assertFalse(app.register("Rugrats Dino", "Reptar", "Tommy Pickles", null));
	}

	/**
	 * Test login.
	 */
	@Test
	public void testLogin() {
		assertTrue(app.login("Rex", "SuperSecret"));
		app.register("Rugrats Dino", "Reptar", "Tommy Pickles", LocalDate.of(1991, 8, 11));	
		assertTrue(app.login("Reptar", "Tommy Pickles"));
		assertFalse(app.login("Reptar", "1234"));
	}


	/**
	 * Test search content.
	 */
	@Test
	public void testSearchContent() {
		app.searchContent("So");
	}

	/**
	 * Test search song.
	 */
	@Test
	public void testSearchSong() {
		List<Song> searchResult = app.searchSong("So");
		app.register("Rugrats Dino", "Reptar", "Tommy Pickles", LocalDate.of(1991, 8, 11));	
		app.login("Reptar", "Tommy Pickles");
		app.upload("./testSong/song1.mp3", "Song1");
		searchResult = app.searchSong("So");
		assertNotNull(searchResult);
		if(!searchResult.isEmpty()) {
			fail("Result should be empty");
		}
		app.login("Rex", "SuperSecret");
		searchResult = app.searchSong("So");
		assertNotNull(searchResult);
		if(searchResult.isEmpty()) {
			fail("Result shoul not be empty");
		}
	}
	
	/**
	 * Test search album.
	 */
	@Test
	public void testSearchAlbum() {
		List<Album> searchResult;
		Map<String, String> canciones = new TreeMap<>();
		app.register("Rugrats Dino", "Reptar", "Tommy Pickles", LocalDate.of(1991, 8, 11));	
		app.login("Reptar", "Tommy Pickles");
		canciones.put("./testSong/song1.mp3", "Song1");
		canciones.put("./testSong/song2.mp3", "Song2");
		app.upload("Test", canciones);
		searchResult = app.searchAlbum("Te");
		assertNotNull(searchResult);
		if(searchResult.isEmpty()) {
			fail("Result shoul not be empty");
		}
		searchResult = app.searchAlbum("PX32");
		assertNotNull(searchResult);
		if(!searchResult.isEmpty()) {
			fail("Result shoul be empty");
		}
	}

	/**
	 * Test search profile.
	 */
	@Test
	public void testSearchProfile() {
		List<Registered> searchResult = app.searchProfile("Rep");
		app.register("Rugrats Dino", "Reptar", "Tommy Pickles", LocalDate.of(1991, 8, 11));
		searchResult = app.searchProfile("Rep");
		assertNotNull(searchResult);
		if(searchResult.isEmpty()) {
			fail("Result shoul not be empty");
		}
	}

	/**
	 * Test delete album.
	 */
	@Test
	public void testDeleteAlbum() {
		Album albumTest;
		Map<String, String> canciones = new TreeMap<>();
		app.register("Rugrats Dino", "Reptar", "Tommy Pickles", LocalDate.of(1991, 8, 11));	
		app.login("Reptar", "Tommy Pickles");
		canciones.put("./testSong/song1.mp3", "Song1");
		canciones.put("./testSong/song2.mp3", "Song2");
		canciones.put("./testSong/song3.mp3", "Song3");
		app.upload("Test", canciones);
		albumTest = app.searchAlbum("Te").get(0);
		app.register("TestName", "TestNick", "TestPass", LocalDate.now());	
		app.login("TestNick", "TestPass");
		assertFalse(app.delete(albumTest));
		app.login("Reptar", "Tommy Pickles");
		assertTrue(app.delete(albumTest));
	}

	/**
	 * Test delete song.
	 */
	@Test
	public void testDeleteSong() {
		Song songTest;
		app.register("Rugrats Dino", "Reptar", "Tommy Pickles", LocalDate.of(1991, 8, 11));	
		app.login("Reptar", "Tommy Pickles");
		app.upload("./testSong/song1.mp3", "Song1");
		app.login("Rex", "SuperSecret");
		songTest = app.searchSong("So").get(0);
		app.register("TestName", "TestNick", "TestPass", LocalDate.now());	
		app.login("TestNick", "TestPass");
		assertFalse(app.delete(songTest));
		app.login("Reptar", "Tommy Pickles");
		assertTrue(app.delete(songTest));
	}

	/**
	 * Test upload string map of string string.
	 */
	@Test
	public void testUploadStringMapOfStringString() {
		Map<String, String> canciones = new TreeMap<>();
		app.register("Rugrats Dino", "Reptar", "Tommy Pickles", LocalDate.of(1991, 8, 11));	
		app.login("Reptar", "Tommy Pickles");
		canciones.put("./testSong/song1.mp3", "Song1");
		canciones.put("./testSong/song2.mp3", "Song2");
		assertTrue(app.upload("Test", canciones));
	}

	/**
	 * Test upload string string.
	 */
	@Test
	public void testUploadStringString() {
		app.register("Rugrats Dino", "Reptar", "Tommy Pickles", LocalDate.of(1991, 8, 11));	
		app.login("Reptar", "Tommy Pickles");
		assertTrue(app.upload("./testSong/song1.mp3", "Song1"));
	}

	/**
	 * Test reupload.
	 */
	@Test
	public void testReupload() {
		Registered testUser = new Registered ("testName", "testNick", "testPass", LocalDate.now());
		Song testSong = new Song("fail.mp3", "fail", testUser);
		Song userUploadedSong;
		app.register("Rugrats Dino", "Reptar", "Tommy Pickles", LocalDate.of(1991, 8, 11));	
		app.login("Reptar", "Tommy Pickles");
		app.upload("./testSong/song1.mp3", "Song1");
		app.login("Rex", "SuperSecret");
		userUploadedSong = app.searchSong("So").get(0);
		assertFalse(app.reupload(userUploadedSong));
		app.login("Reptar", "Tommy Pickles");
		assertFalse(app.reupload(userUploadedSong));
		assertFalse(app.reupload(testSong));
		app.login("Rex", "SuperSecret");
		app.validate(app.getTickets().get(0), false, false);
		app.login("Reptar", "Tommy Pickles");
		assertTrue(app.reupload(userUploadedSong));
	}

	/**
	 * Test validate.
	 */
	@Test
	public void testValidate() {
		List<Ticket> tickets;
		Map<String, String> canciones = new TreeMap<>();
		app.register("Rugrats Dino", "Reptar", "Tommy Pickles", LocalDate.of(1991, 8, 11));	
		app.login("Reptar", "Tommy Pickles");
		canciones.put("./testSong/song2.mp3", "Song2");
		app.upload("Test", canciones);
		app.upload("./testSong/song1.mp3", "Song1");
		app.login("Rex", "SuperSecret");
		tickets = app.getTickets();
		assertTrue(app.validate(tickets.get(0), false, false));
		app.login("Reptar", "Tommy Pickles");
		assertFalse(app.validate(tickets.get(0), false, false));
		app.login("Rex", "SuperSecret");
		assertTrue(app.validate(tickets.get(0), true, true));	
	}

	/**
	 * Test report.
	 */
	@Test
	public void testReport() {
		app.register("Rugrats Dino", "Reptar", "Tommy Pickles", LocalDate.of(1991, 8, 11));	
		app.login("Reptar", "Tommy Pickles");
		app.upload("./testSong/song1.mp3", "Song1");
		app.login("Rex", "SuperSecret");
		assertFalse(app.checkReport(app.getTickets().get(0), true));
		app.validate(app.getTickets().get(0), true, false);
		assertFalse(app.report(app.searchSong("So").get(0)));
		app.register("TestName", "TestNick", "TestPass", LocalDate.now());	
		app.login("TestNick", "TestPass");
		assertTrue(app.report(app.searchSong("So").get(0)));
		
		
	}

	/**
	 * Test check report.
	 */
	@Test
	public void testCheckReport() {
		app.register("Rugrats Dino", "Reptar", "Tommy Pickles", LocalDate.of(1991, 8, 11));	
		app.login("Reptar", "Tommy Pickles");
		app.upload("./testSong/song1.mp3", "Song1");
		app.login("Rex", "SuperSecret");
		app.validate(app.getTickets().get(0), true, false);
		app.register("TestName", "TestNick", "TestPass", LocalDate.now());	
		app.login("TestNick", "TestPass");
		app.report(app.searchSong("So").get(0));
		app.login("Rex", "SuperSecret");
		assertTrue(app.checkReport(app.getTickets().get(0), true));
		
	}

	/**
	 * Test play.
	 */
	@Test
	public void testPlay() {
		Song searchedSong;
		app.register("Rugrats Dino", "Reptar", "Tommy Pickles", LocalDate.of(1991, 8, 11));	
		app.login("Reptar", "Tommy Pickles");
		app.upload("./testSong/song1.mp3", "Song1");
		app.login("Rex", "SuperSecret");
		searchedSong = app.searchSong("So").get(0);
		assertTrue(app.play(searchedSong));
		app.login("Reptar", "Tommy Pickles");
		assertTrue(app.play(searchedSong));
		app.register("TestName", "TestNick", "TestPass", LocalDate.now());	
		app.login("TestNick", "TestPass");
		assertFalse(app.play(searchedSong));
		app.login("Rex", "SuperSecret");
		app.validate(app.getTickets().get(0), true, true);
		app.login("TestNick", "TestPass");
		assertFalse(app.play(searchedSong));
	}
	
	/**
	 * Test follow.
	 */
	@Test
	public void testFollow() {
		app.register("Rugrats Dino", "Reptar", "Tommy Pickles", LocalDate.of(1991, 8, 11));	
		app.login("Reptar", "Tommy Pickles");
		app.register("TestName", "TestNick", "TestPass", LocalDate.now());	
		app.login("TestNick", "TestPass");
		assertTrue(app.follow(app.searchProfile("Rep").get(0)));
		assertFalse(app.follow(app.searchProfile("Rep").get(0)));
		
	}
	
	/**
	 * Test unfollow.
	 */
	@Test
	public void testUnfollow() {
		app.register("Rugrats Dino", "Reptar", "Tommy Pickles", LocalDate.of(1991, 8, 11));	
		app.login("Reptar", "Tommy Pickles");
		app.register("TestName", "TestNick", "TestPass", LocalDate.now());	
		app.login("TestNick", "TestPass");
		assertFalse(app.unfollow(app.searchProfile("Rep").get(0)));
		app.follow(app.searchProfile("Rep").get(0));
		assertTrue(app.unfollow(app.searchProfile("Rep").get(0)));
	}
	
	/**
	 * Testdelete save file.
	 */
	@Test
	public void testdeleteSaveFile() {
		app.disconnect();
		assertTrue(System.deleteSaveFile());
		assertFalse(System.deleteSaveFile());
	}
	
	/**
	 * Test become premium string.
	 */
	@Test
	public void testBecomePremiumString() {
		assertFalse(app.becomePremium("3566002020360508"));
		app.register("Rugrats Dino", "Reptar", "Tommy Pickles", LocalDate.of(1991, 8, 11));	
		app.login("Reptar", "Tommy Pickles");
		assertFalse(app.becomePremium("356600202036050"));
		assertTrue(app.becomePremium("3566002020360508"));
    }
    
	/**
	 * Test become premium.
	 */
	@Test
	public void testBecomePremium() {
		assertFalse(app.becomePremium());
		app.register("Rugrats Dino", "Reptar", "Tommy Pickles", LocalDate.of(1991, 8, 11));	
		app.login("Reptar", "Tommy Pickles");
		assertFalse(app.becomePremium());
    }	
	
	/**
	 * Tear down.
	 */
	@After
	public void tearDown() {
		System.deleteSaveFile();
	}

}
