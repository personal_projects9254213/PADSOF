package es.uam.eps.sadp.p3;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;


/**
 * The Class UnregisteredTest.
 */
public class UnregisteredTest {
	private Unregistered a1;
	private Registered fo;	
	private Song song1;
	private Song song2;

	/**
	 * Sets the up.
	 */
	@Before
	public void setUp() {
		a1 = new Unregistered();
		fo = new Registered("name1", "nick1", "password1", LocalDate.of(1999,6,28));
		song1 = new Song("./testSong/song1.mp3","goodbunny",fo);
		song2 = new Song("./testSong/song2.mp3","goodbunny",fo);
	}
	
	/**
	 * Test getand set listened songs.
	 */
	@Test
	public final void testGetandSetListenedSongs() {
		a1.setListenedSongs(4);
		assertEquals(a1.getListenedSongs(), 4);
	}
	
	/**
	 * Test play.
	 */
	@Test
	public void testPlay() {
		song1.validate(true, true, LocalDate.of(1999,6,28));
		song2.validate(true, false, LocalDate.of(2007,4,9));
		a1.setListenedSongs(Unregistered.MAXSONGS - 1);
		try{
			assertFalse(a1.play(song1));
			assertTrue(a1.play(song2));
			assertFalse(a1.play(song2));
		}
		catch(Exception e){
			
		}
	}

}
