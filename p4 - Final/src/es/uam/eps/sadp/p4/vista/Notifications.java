package es.uam.eps.sadp.p4.vista;

import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import java.util.List;

import javax.swing.*;
import es.uam.eps.sadp.p3.*;
import es.uam.eps.sadp.p3.Ticket.TicketType;

/**
 * Class Notifications
 * This class is the notification page
 * @author <a href="mailto:nicolas.serranos@estudiante.uam.es">Nicolas Serrano</a>
 * @author <a href="mailto:miguela.luque@estudiante.uam.es">Miguel Angel Luque</a>
 * @author <a href="mailto:blanca.mercado@estudiante.uam.es">Blanca Mercado</a>
 * Practice group: 2291
 */
public class Notifications extends JPanel {
	private JLabel label;
	private Gui gui;
	public JList list;
	public DefaultListModel listModel;
	List<Ticket> ticket = new ArrayList<>();


	public Notifications(Gui gui) {
		this.gui = gui;
		setSize(400,400);
		setLayout(null);


		label= new JLabel("Notifications");
		label.setBounds(20,0,220,30);
		label.setFont(new Font("", Font.PLAIN,  22));

		this.add(label);
		Song cancion = null;
		for(int i = 0; i < 10; i++) {
			ticket.add(new Ticket(TicketType.NOTIFICATION, cancion));
		}
		listModel = new DefaultListModel();
		list = new JList(listModel);
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.setLayoutOrientation(JList.VERTICAL);
		list.setSelectedIndex(-1);
		list.setBorder(BorderFactory.createLineBorder(Color.lightGray, 2));
		JScrollPane listView = new JScrollPane(list);
		listView.setBounds(20, 40, 380, 340);
		this.add(listView);

	}
	public void addNotification(List<Ticket> t) {
		listModel.clear();
		for(Ticket currentTicket : t) {
			listModel.addElement(currentTicket);
		}
	}
}
