package es.uam.eps.sadp.p4.vista;

import es.uam.eps.sadp.p4.vista.figuras.*;
import es.uam.eps.sadp.p3.System;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

/**
 * Class payPopUp
 * This class creates a JDialog for introducing credit card for becoming premium
 * @author <a href="mailto:nicolas.serranos@estudiante.uam.es">Nicolas Serrano</a>
 * @author <a href="mailto:miguela.luque@estudiante.uam.es">Miguel Angel Luque</a>
 * @author <a href="mailto:blanca.mercado@estudiante.uam.es">Blanca Mercado</a>
 * Practice group: 2291
 */
public class payPopUp extends JDialog {
	private JLabel label;
	public static JTextField campo;
	public static JButton boton;
	
	public payPopUp(System app) {
		super.setResizable(false);
		super.setSize(400,225);
		super.setLocationRelativeTo(null);
		setModalityType(Dialog.DEFAULT_MODALITY_TYPE);
		
		
		setLayout(null);
		label= new JLabel("Enter your credit card data:");
		label.setBounds(40,20,300,40);
		label.setFont(new Font("Arial", Font.PLAIN,  18));
		
		
		campo = new JTextField(20);
		campo.setBounds(40,80,200,20);
		
		boton = new RoundRectButton("Enter");
		boton.setBounds(250,80,70,20);
		
		
		boton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(app.becomePremium(campo.getText())) {
					JOptionPane.showMessageDialog(null, "Welcome to the Jurassic Park");
					dispose();
				}
			}
		});
		
		
		add(label);
		add(campo);
		add(boton);
		
		super.setVisible(true);
	}
	
}