package es.uam.eps.sadp.p4.vista;

import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.*;

import es.uam.eps.sadp.p4.vista.figuras.MenuButton;

/**
 * Class RegisterMenuBar
 * Menu bar of the register user
 * @author <a href="mailto:nicolas.serranos@estudiante.uam.es">Nicolas Serrano</a>
 * @author <a href="mailto:miguela.luque@estudiante.uam.es">Miguel Angel Luque</a>
 * @author <a href="mailto:blanca.mercado@estudiante.uam.es">Blanca Mercado</a>
 * Practice group: 2291
 */
public class RegisterMenuBar extends JPanel {
	private Gui gui;
	public JButton mainPage, profile, playlists, notifications, upload, disconnect;
	private JLabel logo;
	private Image logoImage;

	public RegisterMenuBar(Gui gui) {
		this.gui = gui;
		setSize(175,525);
		setLayout(null);


		try {
			logoImage = ImageIO.read(new File("./images/logo.gif"));
		} catch (IOException e) {
		}

		logo = new JLabel(new ImageIcon(logoImage.getScaledInstance(175, 175, Image.SCALE_SMOOTH)));
		logo.setBounds(0, 25, 175, 175);
		add(logo);

		mainPage = new MenuButton("Main page");
		mainPage.setBounds(40, 210, 135, 30);
		add(mainPage);

		profile = new MenuButton("Profile");
		profile.setBounds(40, 250, 135, 30);
		add(profile);

		playlists = new MenuButton("Playlists");
		playlists.setBounds(40, 290, 135, 30);
		add(playlists);

		notifications = new MenuButton("Notifications");
		notifications.setBounds(40, 330, 135, 30);
		add(notifications);


		upload = new MenuButton("Upload music");
		upload.setBounds(40, 370, 135, 30);
		add(upload);

		disconnect = new MenuButton("Disconnect");
		disconnect.setBounds(40, 410, 135, 30);
		add(disconnect);
	}
}