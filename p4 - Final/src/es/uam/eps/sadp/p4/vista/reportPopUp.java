package es.uam.eps.sadp.p4.vista;

import java.awt.Dialog;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import es.uam.eps.sadp.p3.Ticket;
import es.uam.eps.sadp.p4.vista.figuras.RoundRectButton;

/**
 * Class reportPopUp
 * This class creates a JDialog for solving a report
 * @author <a href="mailto:nicolas.serranos@estudiante.uam.es">Nicolas Serrano</a>
 * @author <a href="mailto:miguela.luque@estudiante.uam.es">Miguel Angel Luque</a>
 * @author <a href="mailto:blanca.mercado@estudiante.uam.es">Blanca Mercado</a>
 * Practice group: 2291
 */
public class reportPopUp extends JDialog {
	private JLabel label;
	public JButton accept;
	public JButton reject;
	public JButton play;

	public reportPopUp(es.uam.eps.sadp.p3.System app,Ticket t) {
		super.setResizable(false);
		super.setSize(400,225);
		super.setLocationRelativeTo(null);
		setModalityType(Dialog.DEFAULT_MODALITY_TYPE);

		setLayout(null);
		label= new JLabel("Ban");
		label.setBounds(40,40,300,40);
		label.setFont(new Font("", Font.PLAIN,  18));
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setVerticalAlignment(SwingConstants.CENTER);

		play = new RoundRectButton("Play");
		play.setBounds(160,90,80,20);

		accept = new RoundRectButton("Accept");
		accept.setBounds(100,120,80,20);

		reject = new RoundRectButton("Reject");
		reject.setBounds(220,120,80,20);

		play.addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						app.play(t.getSong());
					}
				}
				);

		accept.addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						app.stop();
						app.checkReport(t,true);
						dispose();
					}
				}
				);


		reject.addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent g) {
						app.stop();
						app.checkReport(t,false);
						dispose();
					}
				}
				);

		add(play);
		add(label);
		add(accept);
		add(reject);

		/* Stops song when window is closed*/
		super.addWindowListener(new WindowAdapter(){
			@Override
			public void windowClosing(WindowEvent e)
			{
				app.stop();				
				e.getWindow().dispose();
			}
		});

		setVisible(true);
	}
}