package es.uam.eps.sadp.p4.vista.figuras;

import java.awt.Dimension;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.SwingConstants;

/**
 * Class MenuButton
 * This class creates the buttons for the menu bars
 * @author <a href="mailto:nicolas.serranos@estudiante.uam.es">Nicolas Serrano</a>
 * @author <a href="mailto:miguela.luque@estudiante.uam.es">Miguel Angel Luque</a>
 * @author <a href="mailto:blanca.mercado@estudiante.uam.es">Blanca Mercado</a>
 * Practice group: 2291
 */
public class MenuButton extends JButton {

	public MenuButton(String text){
		super(text);
		Dimension size = getPreferredSize();
		size.width = size.height = Math.max(size.width, size.height);
		setPreferredSize(size);
		setBorder(null);
		setContentAreaFilled(false);
		setFont(new Font("SansSerif", Font.BOLD, 16));
		setHorizontalAlignment(SwingConstants.LEFT);
	}

}
