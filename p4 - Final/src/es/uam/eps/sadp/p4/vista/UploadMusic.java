package es.uam.eps.sadp.p4.vista;

import java.awt.Color;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import es.uam.eps.sadp.p4.vista.figuras.RoundRectButton;

/**
 * Class UploadMusic
 * This class creates the page in charge of uploading an album or song
 * @author <a href="mailto:nicolas.serranos@estudiante.uam.es">Nicolas Serrano</a>
 * @author <a href="mailto:miguela.luque@estudiante.uam.es">Miguel Angel Luque</a>
 * @author <a href="mailto:blanca.mercado@estudiante.uam.es">Blanca Mercado</a>
 * Practice group: 2291
 */
public class UploadMusic extends JPanel {
	private Gui gui;
	private JLabel label;
	public JButton upload;
	public JTextArea log;
	public JFileChooser fc;


	public UploadMusic(Gui gui) {
		this.gui = gui;
		setSize(400,400);
		setLayout(null);

		label = new JLabel ("Upload Music");
		label.setBounds(20,0,220,30);
		label.setFont(new Font("", Font.PLAIN,  22));
		this.add(label);

		upload =  new RoundRectButton("Upload");
		upload.setBounds(320,160,80,20);
		this.add(upload);


		log = new JTextArea(20,10);
		log.setEditable(false);
		log.setBounds(40,160,280,20);
		log.setFont(new Font("", Font.PLAIN,  16));
		log.setBorder(BorderFactory.createLineBorder(Color.lightGray, 1));
		this.add(log);

		//Create a file chooser
		fc = new JFileChooser();
		fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);

	}
}
