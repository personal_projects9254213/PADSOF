package es.uam.eps.sadp.p4.vista.figuras;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.JButton;

/**
 * Class SaurusButton
 * This class creates the buttons for becoming premium
 * @author <a href="mailto:nicolas.serranos@estudiante.uam.es">Nicolas Serrano</a>
 * @author <a href="mailto:miguela.luque@estudiante.uam.es">Miguel Angel Luque</a>
 * @author <a href="mailto:blanca.mercado@estudiante.uam.es">Blanca Mercado</a>
 * Practice group: 2291
 */
public class SaurusButton extends JButton {

	public SaurusButton(String text){
		super(text);
		Dimension size = getPreferredSize();
		size.width = size.height = Math.max(size.width, size.height);
		setPreferredSize(size);
		setBorder(null);
		setBackground(Color.BLACK);
		setFont(new Font("SansSerif", Font.BOLD+Font.ITALIC, 14));
		setForeground(Color.YELLOW);
	}

}
