package es.uam.eps.sadp.p4.vista;

import javax.swing.JButton;
import javax.swing.JPanel;
import es.uam.eps.sadp.p4.vista.figuras.SaurusButton;

/**
 * Class BecomeSaurus
 * This class creates the button Become a Saurus, in order to become a premium user
 * @author <a href="mailto:nicolas.serranos@estudiante.uam.es">Nicolas Serrano</a>
 * @author <a href="mailto:miguela.luque@estudiante.uam.es">Miguel Angel Luque</a>
 * @author <a href="mailto:blanca.mercado@estudiante.uam.es">Blanca Mercado</a>
 * Practice group: 2291
 */
public class BecomeSaurus extends JPanel {
	public JButton premium;
	private Gui gui;

	public BecomeSaurus(Gui gui) {
		this.gui = gui;

		setSize(150,20);
		setLayout(null);

		premium = new SaurusButton("Become a Saurus");
		premium.setBounds(0,0,150,20);

		this.add(premium);
	}

}