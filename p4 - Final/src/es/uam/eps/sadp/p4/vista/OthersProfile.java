package es.uam.eps.sadp.p4.vista;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreeSelectionModel;

import es.uam.eps.sadp.p3.Album;
import es.uam.eps.sadp.p3.Registered;
import es.uam.eps.sadp.p3.Song;
import es.uam.eps.sadp.p4.vista.figuras.RoundRectButton;
import es.uam.eps.sadp.p4.vista.figuras.SaurusButton;

/**
 * Class OthersProfile
 * This class allows the user see another user profile
 * @author <a href="mailto:nicolas.serranos@estudiante.uam.es">Nicolas Serrano</a>
 * @author <a href="mailto:miguela.luque@estudiante.uam.es">Miguel Angel Luque</a>
 * @author <a href="mailto:blanca.mercado@estudiante.uam.es">Blanca Mercado</a>
 * Practice group: 2291
 */
public class OthersProfile extends JPanel {
	private JLabel label;
	private Gui gui;
	public JTree tree;
	public JButton follow;
	public JButton unfollow;
	private DefaultTreeModel treeModel;
	private MutableTreeNode rootNode;
	public Registered owner = null;

	public OthersProfile(Gui gui) {
		this.gui = gui;
		setSize(400,400);
		setLayout(null);


		label= new JLabel("Others Nick");
		label.setBounds(20,0,220,30);
		label.setFont(new Font("", Font.PLAIN,  22));

		this.add(label);

		follow = new RoundRectButton("Follow");
		follow.setBounds(300,5,100,20);
		this.add(follow);
		follow.setVisible(false);

		follow.addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						follow.setVisible(false);
						unfollow.setVisible(true);
					}
				}
				);

		unfollow = new RoundRectButton("Unfollow");
		unfollow.setBounds(300,5,100,20);
		unfollow.setVisible(false);
		this.add(unfollow);

		unfollow.addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						unfollow.setVisible(false);
						follow.setVisible(true);
					}
				}
				);

		rootNode = new DefaultMutableTreeNode("Root Node");
		treeModel = new DefaultTreeModel(rootNode);

		DefaultMutableTreeNode otherNode = new DefaultMutableTreeNode("Otro");
		rootNode.insert(otherNode, 0);

		tree = new JTree(treeModel);
		tree.setEditable(false);
		tree.getSelectionModel().setSelectionMode
		(TreeSelectionModel.SINGLE_TREE_SELECTION);
		tree.setShowsRootHandles(true);
		tree.setBorder(BorderFactory.createLineBorder(Color.lightGray, 2));
		JScrollPane treeView = new JScrollPane(tree);
		treeView.setBounds(20, 40, 380, 340);
		this.add(treeView);

	}
	public void setData(Registered r) {
		owner = r;
		clearTree(tree);
		label.setText(r.getNick());
		DefaultMutableTreeNode otherNode,parentNode;
		if(r.getSongs().size()>0) {
			parentNode = new DefaultMutableTreeNode("Singles");
			rootNode.insert(parentNode, 0);
			for(Song s: r.getSongs()) {
				otherNode = new DefaultMutableTreeNode(s);
				addNodeToDefaultTreeModel(treeModel,parentNode,otherNode);
			}
		}
		for(Album a: r.getAlbums()) {
			parentNode = new DefaultMutableTreeNode(a);
			rootNode.insert(parentNode, 0);
			for(Song s: a.getSongs()) {
				otherNode = new DefaultMutableTreeNode(s);
				addNodeToDefaultTreeModel(treeModel,parentNode,otherNode);
			}
		}
	}
	private void clearTree(JTree tree) {
		if (tree.toString() == null) { return; }
		DefaultTreeModel model = (DefaultTreeModel) tree.getModel();
		DefaultMutableTreeNode root = (DefaultMutableTreeNode) model.getRoot();
		root.removeAllChildren();
		model.reload();
	}
	private static void addNodeToDefaultTreeModel( DefaultTreeModel treeModel, DefaultMutableTreeNode parentNode, DefaultMutableTreeNode node ) {

		treeModel.insertNodeInto(  node, parentNode, parentNode.getChildCount()  );

		if (  parentNode == treeModel.getRoot()  ) {
			treeModel.nodeStructureChanged(  (TreeNode) treeModel.getRoot()  );
		}
	}
}