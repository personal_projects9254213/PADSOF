package es.uam.eps.sadp.p4.vista.figuras;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

/**
 * Class MyComboBoxRenderer
 * This class creates the ComboBoxRenderer for playlistComboBox
 * @author <a href="mailto:nicolas.serranos@estudiante.uam.es">Nicolas Serrano</a>
 * @author <a href="mailto:miguela.luque@estudiante.uam.es">Miguel Angel Luque</a>
 * @author <a href="mailto:blanca.mercado@estudiante.uam.es">Blanca Mercado</a>
 * Practice group: 2291
 */
public class MyComboBoxRenderer extends JLabel implements ListCellRenderer {
	private String title;

	public MyComboBoxRenderer(String title) {
		this.title = title;
	}

	@Override
	public MyComboBoxRenderer getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean hasFocus) {
		if (index == -1 && value == null) setText(title);
		else setText(value.toString());
		return this;
	}
}
