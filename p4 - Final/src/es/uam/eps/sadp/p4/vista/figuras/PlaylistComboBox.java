package es.uam.eps.sadp.p4.vista.figuras;

import javax.swing.JComboBox;

/**
 * Class PlaylistComboBox
 * This class creates the comboBox adding to a playlist option
 * @author <a href="mailto:nicolas.serranos@estudiante.uam.es">Nicolas Serrano</a>
 * @author <a href="mailto:miguela.luque@estudiante.uam.es">Miguel Angel Luque</a>
 * @author <a href="mailto:blanca.mercado@estudiante.uam.es">Blanca Mercado</a>
 * Practice group: 2291
 */
public class PlaylistComboBox extends JComboBox<String>{

	public PlaylistComboBox() {
		super();
		addItem("New Playlist");
		setBounds(0,0,140,20);
		setRenderer(new MyComboBoxRenderer("Add to Playlist"));
		setSelectedIndex(-1);

	}

}
