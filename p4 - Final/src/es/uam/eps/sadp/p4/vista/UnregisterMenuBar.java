package es.uam.eps.sadp.p4.vista;

import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.*;

import es.uam.eps.sadp.p4.vista.figuras.MenuButton;

/**
 * Class UnregisterMenuBar
 * Menu bar of the unregister user
 * @author <a href="mailto:nicolas.serranos@estudiante.uam.es">Nicolas Serrano</a>
 * @author <a href="mailto:miguela.luque@estudiante.uam.es">Miguel Angel Luque</a>
 * @author <a href="mailto:blanca.mercado@estudiante.uam.es">Blanca Mercado</a>
 * Practice group: 2291
 */
public class UnregisterMenuBar extends JPanel {
	private Gui gui;
	private JButton mainPage;
	public JButton login;
	private JLabel logo;
	private Image logoImage;

	public UnregisterMenuBar(Gui gui) {
		this.gui = gui;
		setSize(175,525);
		setLayout(null);


		try {
			logoImage = ImageIO.read(new File("./images/logo.gif"));
		} catch (IOException e) {
		}

		logo = new JLabel(new ImageIcon(logoImage.getScaledInstance(175, 175, Image.SCALE_SMOOTH)));
		logo.setBounds(0, 25, 175, 175);
		add(logo);

		mainPage = new MenuButton("Main page");
		mainPage.setBounds(40, 210, 135, 30);
		add(mainPage);
		mainPage.addActionListener(event -> {
			gui.setAllNotVisibleExceptPlayZones();
			gui.unregisteredMenuBar.setVisible(true);
			gui.mainPage.setVisible(true);
		});

		login = new MenuButton("Login");
		login.setBounds(40, 250, 135, 30);
		add(login);


	}
}