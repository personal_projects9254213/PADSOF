package es.uam.eps.sadp.p4.vista;

import java.awt.Color;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import es.uam.eps.sadp.p4.vista.figuras.RoundRectButton;
import es.uam.eps.sadp.p4.vista.figuras.SearchComboBox;

/**
 * Class FirstSearch
 * This class creates the main page in charge of searching
 * @author <a href="mailto:nicolas.serranos@estudiante.uam.es">Nicolas Serrano</a>
 * @author <a href="mailto:miguela.luque@estudiante.uam.es">Miguel Angel Luque</a>
 * @author <a href="mailto:blanca.mercado@estudiante.uam.es">Blanca Mercado</a>
 * Practice group: 2291
 */
public class FirstSearch extends JPanel {
	private Gui gui;
	public JTextField field;
	public JButton search;
	public JComboBox filter;

	public FirstSearch(Gui gui) {
		this.gui = gui;
		setSize(400,400);
		setLayout(null);

		JLabel label = new JLabel ("Dino's Music");
		label.setBounds(50,100, 300, 40);
		label.setFont(new Font("", Font.PLAIN,  32));
		label.setHorizontalAlignment(SwingConstants.CENTER);
		this.add(label);

		field = new JTextField(280);
		field.setBounds(30,160,220,20);
		field.setBorder(null);
		field.setFont(new Font("", Font.PLAIN,  16));
		field.setBorder(BorderFactory.createLineBorder(Color.lightGray, 1));
		this.add(field);

		filter = new SearchComboBox();
		filter.setLocation(250,160);
		this.add(filter);

		search = new RoundRectButton("Search");
		search.setBounds(320,160,80,20);
		this.add(search);

	}
}

