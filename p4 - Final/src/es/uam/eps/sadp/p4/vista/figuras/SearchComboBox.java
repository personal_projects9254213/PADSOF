package es.uam.eps.sadp.p4.vista.figuras;

import javax.swing.JComboBox;

/**
 * Class SearchComboBox
 * This class creates the comboBox to filter a search
 * @author <a href="mailto:nicolas.serranos@estudiante.uam.es">Nicolas Serrano</a>
 * @author <a href="mailto:miguela.luque@estudiante.uam.es">Miguel Angel Luque</a>
 * @author <a href="mailto:blanca.mercado@estudiante.uam.es">Blanca Mercado</a>
 * Practice group: 2291
 */
public class SearchComboBox extends JComboBox<String>{
	final static String[] options = { "User", "Song", "Album"};

	public SearchComboBox() {
		super(options);
		setBounds(0,0,70,20);
		setRenderer(new MyComboBoxRenderer("Filter"));
		setSelectedIndex(-1);

	}

}
