package es.uam.eps.sadp.p4.vista;
import java.awt.Container;
import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

/**
 * Class Gui
 * This class is the main view JFrame including the rest of the components a JPanels
 * @author <a href="mailto:nicolas.serranos@estudiante.uam.es">Nicolas Serrano</a>
 * @author <a href="mailto:miguela.luque@estudiante.uam.es">Miguel Angel Luque</a>
 * @author <a href="mailto:blanca.mercado@estudiante.uam.es">Blanca Mercado</a>
 * Practice group: 2291
 */
public class Gui extends JFrame {
	protected RegisterMenuBar registerMenuBar;
	protected UnregisterMenuBar unregisteredMenuBar;
	protected AdminMenuBar adminMenuBar;
	protected SearchGui search;
	protected BecomeSaurus saurus;
	protected FirstSearch mainPage;
	protected SearchedList searchedList;
	protected SearchedList playlistList;
	protected Login login;
	protected Register signin;
	protected UserProfile userProfile;
	protected OthersProfile othersProfile;
	protected Notifications notifications;
	protected UploadMusic uploadMusic;
	protected PlayZone playZone;
	protected PlayZone playlistZone;
	private Image logoImage;

	public Gui() {
		super("Dino's Music");
		super.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		super.setResizable(false);
		super.setSize(900,525);
		super.setLocationRelativeTo(null);
		try {
			logoImage = ImageIO.read(new File("./images/logo.gif"));
		} catch (IOException e) {
		}

		setIconImage(logoImage);



		Container contenedor = this.getContentPane();
		contenedor.setLayout(null);

		registerMenuBar = new RegisterMenuBar(this);
		registerMenuBar.setLocation(0, 0);
		unregisteredMenuBar = new UnregisterMenuBar(this);
		unregisteredMenuBar.setLocation(0, 0);
		adminMenuBar = new AdminMenuBar(this);
		adminMenuBar.setLocation(0, 0);
		search = new SearchGui(this);
		search.setLocation(175,0);
		saurus = new BecomeSaurus(this);
		saurus.setLocation(740,0);
		mainPage = new FirstSearch(this);
		mainPage.setLocation(175, 65);
		searchedList = new SearchedList(this);
		searchedList.setLocation(175, 65);
		playlistList = new SearchedList(this);
		playlistList.setLocation(175, 65);
		login = new Login(this);
		login.setLocation(175, 65);
		signin = new Register(this);
		signin.setLocation(175,65);
		userProfile = new UserProfile(this);
		userProfile.setLocation(175, 65);
		othersProfile = new OthersProfile(this);
		othersProfile.setLocation(175, 65);
		notifications = new Notifications(this);
		notifications.setLocation(175, 65);
		uploadMusic = new UploadMusic(this);
		uploadMusic.setLocation(175, 65);
		playZone = new PlayZone(this);
		playZone.setLocation(575, 65);
		playlistZone = new PlayZone(this);
		playlistZone.setLocation(575, 65);

		contenedor.add(registerMenuBar);
		contenedor.add(unregisteredMenuBar);
		contenedor.add(adminMenuBar);
		contenedor.add(search);
		contenedor.add(saurus);
		contenedor.add(mainPage);
		contenedor.add(searchedList);
		contenedor.add(login);
		contenedor.add(signin);
		contenedor.add(userProfile);
		contenedor.add(othersProfile);
		contenedor.add(notifications);
		contenedor.add(uploadMusic);
		contenedor.add(playZone);
		contenedor.add(playlistZone);
		contenedor.add(playlistList);



		registerMenuBar.setVisible(false);
		unregisteredMenuBar.setVisible(true);
		adminMenuBar.setVisible(false);
		search.setVisible(true);
		saurus.setVisible(false);
		mainPage.setVisible(false);
		searchedList.setVisible(false);
		login.setVisible(true);
		signin.setVisible(false);
		userProfile.setVisible(false); 
		othersProfile.setVisible(false);
		notifications.setVisible(false);
		uploadMusic.setVisible(false);
		playZone.setVisible(false);
		playlistZone.setVisible(false);
		playlistZone.combo.setVisible(false);
		playlistZone.playlist.setVisible(true);
		playlistList.setVisible(false);
		this.setVisible(true);
		contenedor.repaint();
		contenedor.revalidate();
		this.repaint();
	}

	public void setAllNotVisibleExceptPlayZones() {
		registerMenuBar.setVisible(false);
		unregisteredMenuBar.setVisible(false);
		adminMenuBar.setVisible(false);
		search.setVisible(false);
		saurus.setVisible(false);
		mainPage.setVisible(false);
		searchedList.setVisible(false);
		login.setVisible(false);
		signin.setVisible(false);
		userProfile.setVisible(false); 
		othersProfile.setVisible(false);
		notifications.setVisible(false);
		uploadMusic.setVisible(false);
		playlistList.setVisible(false);
		this.repaint();
	}

	public void setAllNotVisible() {
		registerMenuBar.setVisible(false);
		unregisteredMenuBar.setVisible(false);
		adminMenuBar.setVisible(false);
		search.setVisible(false);
		saurus.setVisible(false);
		mainPage.setVisible(false);
		searchedList.setVisible(false);
		login.setVisible(false);
		signin.setVisible(false);
		userProfile.setVisible(false); 
		othersProfile.setVisible(false);
		notifications.setVisible(false);
		uploadMusic.setVisible(false);
		playZone.setVisible(false);
		playlistZone.setVisible(false);
		playlistList.setVisible(false);
		this.repaint();
	}

}
