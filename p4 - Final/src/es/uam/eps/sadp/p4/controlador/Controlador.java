package es.uam.eps.sadp.p4.controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.tree.DefaultMutableTreeNode;

import es.uam.eps.sadp.p3.Album;
import es.uam.eps.sadp.p3.Playlist;
import es.uam.eps.sadp.p3.Registered;
import es.uam.eps.sadp.p3.Song;
import es.uam.eps.sadp.p3.System;
import es.uam.eps.sadp.p3.Ticket;
import es.uam.eps.sadp.p3.Ticket.TicketType;
import es.uam.eps.sadp.p4.vista.Gui;
import es.uam.eps.sadp.p4.vista.newPlaylistPopUp;
import es.uam.eps.sadp.p4.vista.payPopUp;
import es.uam.eps.sadp.p4.vista.reportPopUp;
import es.uam.eps.sadp.p4.vista.validatePopUp;

/**
 * Class Controlador
 * Contains the action listener connected to the application and the main execution
 * @author <a href="mailto:nicolas.serranos@estudiante.uam.es">Nicolas Serrano</a>
 * @author <a href="mailto:miguela.luque@estudiante.uam.es">Miguel Angel Luque</a>
 * @author <a href="mailto:blanca.mercado@estudiante.uam.es">Blanca Mercado</a>
 * Practice group: 2291
 */
public class Controlador extends Gui {	
	private es.uam.eps.sadp.p3.System app;
	public Controlador(es.uam.eps.sadp.p3.System system) {
		super();
		this.app = system;
		/* Log in*/
		login.login.addActionListener(event -> {
			String pass = String.valueOf(login.passwordRead.getPassword());
			if(app.login(login.usernameRead.getText(),pass)) {
				setAllNotVisible();
				if(app.isAdmin()) {
					notifications.addNotification(app.getTickets());
					adminMenuBar.setVisible(true);
					notifications.setVisible(true);
				}else {
					registerMenuBar.setVisible(true);
					mainPage.setVisible(true);
					if(!app.isPremium()) {
						saurus.setVisible(true);
					}
				}

			}
		});

		/* Sign in*/
		signin.signin.addActionListener(event -> {
			String pass = String.valueOf(signin.passwordRead.getPassword());
			Date selectedDate = (Date) signin.datePicker.getModel().getValue();
			LocalDate date = selectedDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			if(app.register(signin.nameRead.getText(),signin.usernameRead.getText(),pass,date)) {
				if(app.login(signin.usernameRead.getText(), pass)) {
					setAllNotVisible();
					registerMenuBar.setVisible(true);
					saurus.setVisible(true);
					mainPage.setVisible(true);
				}
			}
		});

		/* First search*/
		mainPage.search.addActionListener(event -> {
			if(mainPage.filter.getSelectedItem().equals("User")) {
				searchedList.setList(app.searchProfile(mainPage.field.getText()),"User");
			}
			if(mainPage.filter.getSelectedItem().equals("Song")) {
				searchedList.setList(app.searchSong(mainPage.field.getText()),"Song");
			}
			if(mainPage.filter.getSelectedItem().equals("Album")) {
				searchedList.setList(app.searchAlbum(mainPage.field.getText()),"Album");
			}
			setAllNotVisibleExceptPlayZones();
			if(app.isAdmin()) {
				adminMenuBar.setVisible(true);
			} else if(app.isDefaultUser()) {
				unregisteredMenuBar.setVisible(true);
			} else {
				registerMenuBar.setVisible(true);
				if(!app.isPremium()) {
					saurus.setVisible(true);
				}
			}
			searchedList.comboBox.setVisible(false);
			searchedList.setVisible(true);
			search.setVisible(true);
		});

		/* Search*/
		search.search.addActionListener(event -> {
			if(search.filter.getSelectedItem().equals("User")) {
				searchedList.setList(app.searchProfile(search.field.getText()),"User");
			}
			if(search.filter.getSelectedItem().equals("Song")) {
				searchedList.setList(app.searchSong(search.field.getText()),"Song");
			}
			if(search.filter.getSelectedItem().equals("Album")) {
				searchedList.setList(app.searchAlbum(search.field.getText()),"Album");
			}
			setAllNotVisibleExceptPlayZones();
			if(app.isAdmin()) {
				adminMenuBar.setVisible(true);
			} else if(app.isDefaultUser()) {
				unregisteredMenuBar.setVisible(true);
			} else {
				registerMenuBar.setVisible(true);
				if(!app.isPremium()) {
					saurus.setVisible(true);
				}
			}
			searchedList.comboBox.setVisible(false);
			searchedList.setVisible(true);
			search.setVisible(true);
		});

		/* Display playZone*/
		searchedList.list.addMouseListener(new MouseAdapter() { 
			public void mouseClicked(MouseEvent me) { 
				Song s;
				Album a;
				if(searchedList.type.equals("User")) {
					setAllNotVisibleExceptPlayZones();
					if(!system.isDefaultUser()) {
						Registered rrr = (Registered)app.currentUser();
						othersProfile.follow.setVisible(!rrr.isFollowing(othersProfile.owner));
						othersProfile.unfollow.setVisible(rrr.isFollowing(othersProfile.owner));
						registerMenuBar.setVisible(true);
						if(!app.isPremium()) {
							saurus.setVisible(true);
						}
					} else {
						othersProfile.follow.setVisible(false);
						othersProfile.unfollow.setVisible(false);
						unregisteredMenuBar.setVisible(true);
					}
					if(searchedList.list.getSelectedValue().equals(app.currentUser())) {
						userProfile.setData((Registered)app.currentUser());
						userProfile.setVisible(true);
					} else {
						searchedList.comboBox.setVisible(false);
						othersProfile.setData((Registered)searchedList.list.getSelectedValue());
						othersProfile.setVisible(true);
					}
					search.setVisible(true);

				} else if(searchedList.type.equals("Song")) {
					s = (Song) searchedList.list.getSelectedValue();

					if(!system.isDefaultUser()) {
						playZone.setSong(app,s);
						playZone.report.setVisible(true);
						playZone.combo.setVisible(true);
					} else {
						playZone.setSong(s);
						playZone.report.setVisible(false);
						playZone.combo.setVisible(false);
					}
					playZone.setVisible(true);
				} else if(searchedList.type.equals("Album")) {
					a = (Album) searchedList.list.getSelectedValue();
					if(!system.isDefaultUser()) {
						playZone.a = a;
						searchedList.setList(app,a.getSongs(),"Song", a.getTitle());
						searchedList.comboBox.setVisible(true);
					}else {
						searchedList.setList(a.getSongs(),"Song", a.getTitle());
					}
				}
			} 
		});

		/* Display playlistZone*/
		playlistList.list.addMouseListener(new MouseAdapter() { 
			public void mouseClicked(MouseEvent me) { 
				Playlist p;
				if(playlistList.type.equals("Song")) {
					playlistZone.setSong(app,(Song)playlistList.list.getSelectedValue());
					playlistZone.setVisible(true);
				} else if(playlistList.type.equals("Playlist")) {
					playlistList.comboBox.setVisible(true);
					p = (Playlist)playlistList.list.getSelectedValue();
					playlistZone.actual = p;
					playlistList.setList(app,p.getSongs(),"Song", p.getTitle());
				}
			} 
		});

		/* Add song to playlist */
		playZone.combo.addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						Registered r = (Registered)app.currentUser();
						Song ss = playZone.s;
						String selectedOption = (String)playZone.combo.getSelectedItem();
						if(selectedOption.equals("New Playlist")) {
							new newPlaylistPopUp(playZone.combo,playlistList.comboBox,searchedList.comboBox,r,ss);
						} else {
							r.getPlaylist(selectedOption).addToPlaylist(ss);
						}
					}
				}
				);

		/* Add playlist to playlist*/
		playlistList.comboBox.addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						Registered r = (Registered)app.currentUser();
						String selectedOption = (String)playlistList.comboBox.getSelectedItem();
						if(selectedOption.equals("New Playlist")) {
							new newPlaylistPopUp(playZone.combo,playlistList.comboBox,searchedList.comboBox,r,playlistZone.actual);
						} else {
							r.getPlaylist(selectedOption).addToPlaylist(playlistZone.actual);
						}
					}
				}
				);

		/* Add album to playlist*/
		searchedList.comboBox.addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						Registered r = (Registered)app.currentUser();
						String selectedOption = (String)searchedList.comboBox.getSelectedItem();
						if(selectedOption.equals("New Playlist")) {
							new newPlaylistPopUp(playZone.combo,playlistList.comboBox,searchedList.comboBox,r,playZone.a);
						} else {
							r.getPlaylist(selectedOption).addToPlaylist(playZone.a);
						}
					}
				}
				);


		/* Become premium*/
		saurus.premium.addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						if(system.becomePremium()) {
							JOptionPane.showMessageDialog(null, "Welcome to the Jurassic Park");
						} else {
							new payPopUp(app);
						}
						Registered currentUser = (Registered) system.currentUser();
						if(currentUser.getPremium()) {
							saurus.setVisible(false);
						}
					}
				}
				);

		/* Erase from playlist*/
		playlistZone.playlist.addActionListener(
				new ActionListener() {	
					public void actionPerformed(ActionEvent e) {
						if(playlistZone.actual.getSongs().contains(playlistZone.s)) {
							playlistZone.actual.quitFromPlaylist(playlistZone.s);
							playlistList.setList(app,playlistZone.actual.getSongs(),"Song");
						}
					}
				}
				);



		/* Follow and unfollow*/
		othersProfile.follow.addActionListener(event -> {
			Registered curre;
			curre = (Registered)app.currentUser();
			curre.follow(othersProfile.owner);
		});
		othersProfile.unfollow.addActionListener(event -> {
			Registered curre;
			curre = (Registered)app.currentUser();
			curre.unfollow(othersProfile.owner);
		});

		/* Upload*/
		uploadMusic.upload.addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent g) {
						String path,title;
						Map<String,String> albums = new HashMap<>();
						int returnVal = uploadMusic.fc.showOpenDialog(uploadMusic.fc);

						if (returnVal == JFileChooser.APPROVE_OPTION) {
							File file = uploadMusic.fc.getSelectedFile();
							if(file.isFile()) {
								//This is where a real application would open the file.
								uploadMusic.log.insert("Opening file: " + file.getName() + ".\n", 0);
								String out[] = file.getName().split(Pattern.quote("."));
								title = out[0];
								app.upload(file.getPath(),title);
							} else if(file.isDirectory()){
								uploadMusic.log.insert("Opening directory: " + file.getName() + ".\n", 0);
								for(File subfile: file.listFiles() ) {
									if(subfile.isFile()) {
										path = subfile.getPath();
										String out[] = subfile.getName().split(Pattern.quote("."));
										title = out[0];
										albums.put(path, title);
									}
								}
								app.upload(file.getName(), albums);
							}
						} else {
							uploadMusic.log.append("Open command cancelled by user..\n");
						}
						uploadMusic.log.setCaretPosition(uploadMusic.log.getDocument().getLength());
					}
				}
				);

		/* Register enters main page*/
		registerMenuBar.mainPage.addActionListener(event -> {
			setAllNotVisibleExceptPlayZones();
			registerMenuBar.setVisible(true);
			mainPage.setVisible(true);
			if(!app.isPremium()) {
				saurus.setVisible(true);
			}
		});

		/* Refresh the notifications(for registered) while entering the window*/
		registerMenuBar.notifications.addActionListener(event -> {
			setAllNotVisibleExceptPlayZones();
			notifications.addNotification(app.getTickets());
			registerMenuBar.setVisible(true);
			search.setVisible(true);
			notifications.setVisible(true);
			if(!app.isPremium()) {
				saurus.setVisible(true);
			}
		});

		/* Register enters upload page*/
		registerMenuBar.upload.addActionListener(event -> {
			setAllNotVisibleExceptPlayZones();
			registerMenuBar.setVisible(true);
			search.setVisible(true);
			uploadMusic.setVisible(true);
			if(!app.isPremium()) {
				saurus.setVisible(true);
			}
		});

		/* Admin disconnected */
		adminMenuBar.disconnect.addActionListener(event -> {
			setAllNotVisible();
			unregisteredMenuBar.setVisible(true);
			search.setVisible(true);
			login.setVisible(true);
			app.disconnect();
			//Parar musica
		});

		/* Refresh the profile while entering the window*/
		registerMenuBar.profile.addActionListener(event -> {
			setAllNotVisibleExceptPlayZones();
			userProfile.setData((Registered)app.currentUser());
			registerMenuBar.setVisible(true);
			search.setVisible(true);
			userProfile.setVisible(true);
			if(!app.isPremium()) {
				saurus.setVisible(true);
			}
		});

		/* Display Playlists*/
		registerMenuBar.playlists.addActionListener(event -> {
			Registered uuu = (Registered)app.currentUser();
			setAllNotVisibleExceptPlayZones();
			playlistList.comboBox.setVisible(false);
			playlistList.setList(app,uuu.getPlaylists(),"Playlist");
			registerMenuBar.setVisible(true);
			search.setVisible(true);
			playlistList.setVisible(true);
			if(!app.isPremium()) {
				saurus.setVisible(true);
			}
		});

		/* Register disconnected */
		registerMenuBar.disconnect.addActionListener(event -> {
			setAllNotVisible();
			unregisteredMenuBar.setVisible(true);
			search.setVisible(true);
			login.setVisible(true);
			app.disconnect();
			//Parar musica
		});

		/* Unregistered logs in*/
		unregisteredMenuBar.login.addActionListener(event -> {
			setAllNotVisible();
			unregisteredMenuBar.setVisible(true);
			search.setVisible(true);
			login.setVisible(true);
			app.stop();
		});

		/* Resolver notificaciones*/
		notifications.list.addMouseListener(new MouseAdapter() { 
			public void mouseClicked(MouseEvent me) { 
				Ticket t = (Ticket) notifications.list.getSelectedValue();
				if(t.getType().equals(TicketType.NOTIFICATION)) {
					Registered currentUser = (Registered) app.currentUser();
					currentUser.deleteNotification(t);
				}else if(t.getType().equals(TicketType.VALIDATION)) {
					validatePopUp val = new validatePopUp(app,t);

				}else if(t.getType().equals(TicketType.REPORT)) {
					reportPopUp rep = new reportPopUp(app,t);
				}
				notifications.addNotification(app.getTickets());
			} 
		}); 


		/* Report song*/
		playZone.report.addActionListener(event -> {
			app.report(playZone.s);
		});
		/* Open player from other user*/
		othersProfile.tree.addMouseListener(new MouseAdapter() { 
			public void mouseClicked(MouseEvent me) {
				DefaultMutableTreeNode node = (DefaultMutableTreeNode)othersProfile.tree.getLastSelectedPathComponent();
				if(node != null) {
					if (node.isLeaf()) {
						if(!system.isDefaultUser()) {
							playZone.setSong(app,(Song)node.getUserObject());
							playZone.report.setVisible(true);
							playZone.combo.setVisible(true);
						} else {
							playZone.setSong((Song)node.getUserObject());
							playZone.report.setVisible(false);
							playZone.combo.setVisible(false);
						}
						playZone.setVisible(true);
					}
				}
			}
		});

		/* Open player from profile*/
		userProfile.tree.addMouseListener(new MouseAdapter() { 
			public void mouseClicked(MouseEvent me) {
				DefaultMutableTreeNode node = (DefaultMutableTreeNode)userProfile.tree.getLastSelectedPathComponent();
				if(node != null) {
					if (node.isLeaf()) {
						if(!system.isDefaultUser()) {
							playZone.setSong(app,(Song)node.getUserObject());
							playZone.report.setVisible(true);
							playZone.combo.setVisible(true);
						} else {
							playZone.setSong((Song)node.getUserObject());
							playZone.report.setVisible(false);
							playZone.combo.setVisible(false);
						}
						playZone.setVisible(true);
					}
				}
			}
		});

		/* Play*/
		playZone.play.addActionListener(event -> {
			app.play(playZone.s);
		});

		/* Stop*/
		playZone.stop.addActionListener(event -> {
			app.stop();
		});

		/* Play playlist*/
		playlistZone.play.addActionListener(event -> {
			app.play(playZone.s);
		});

		/* Stop playlist*/
		playlistZone.stop.addActionListener(event -> {
			app.stop();
		});

		/* DeleteSong*/
		userProfile.deletes.addActionListener(event -> {
			DefaultMutableTreeNode node = (DefaultMutableTreeNode)userProfile.tree.getLastSelectedPathComponent();
			if (node.isLeaf()) {
				app.delete((Song)node.getUserObject());
			}else if(!node.getUserObject().equals("Singles")) {
				app.delete((Album)node.getUserObject());
			}
			userProfile.setData((Registered)app.currentUser());
		});

		/* Saves when window is closed*/
		super.addWindowListener(new WindowAdapter()
		{
			@Override
			public void windowClosing(WindowEvent e)
			{
				try {
					app.disconnect();
					app.save();
				} catch(FileNotFoundException e1) {
					java.lang.System.out.println("System savefile missing!");
				} catch(IOException e2) {
					java.lang.System.out.println("System could have not be saved properly");
				}
				e.getWindow().dispose();
			}
		});
	}

	public static void main(String args[]) {
		new Controlador(System.getInstance());
	}
}
