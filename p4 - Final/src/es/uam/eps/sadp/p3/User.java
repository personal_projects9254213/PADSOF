package es.uam.eps.sadp.p3;

import java.io.FileNotFoundException;
import java.io.Serializable;

import pads.musicPlayer.Mp3Player;
import pads.musicPlayer.exceptions.Mp3PlayerException;

/**
 * Class User
 * @author <a href="mailto:nicolas.serranos@estudiante.uam.es">Nicolas Serrano</a>
 * @author <a href="mailto:miguela.luque@estudiante.uam.es">Miguel Angel Luque</a>
 * @author <a href="mailto:blanca.mercado@estudiante.uam.es">Blanca Mercado</a>
 * Practice group: 2291
 */
public abstract class User implements Serializable {
	private static final long serialVersionUID = 1L;
	private String nick;
	private int password;
	public transient static Mp3Player player;
	/**
	 * Constructor of the class User
	 * @param nick of the user
	 * @param password of the user
	 */
	public User(String nick, String password) {
		this.nick = nick;
		this.password = password.hashCode();
		try {
			player = new Mp3Player();
		}catch(Mp3PlayerException exc) {
			player = null;
		}catch(FileNotFoundException exc2) {
			player = null;
		}
	}

	public String getNick() {
		return nick;
	}

	public int getPassword() {
		return password;
	}

	/**
	 * Plays the song passed by argument
	 * @param song song wanted to play
	 * @return boolean saying if the song have been reproducted
	 * @exception FileNotFoundException if file does not exist.
	 * @exception Mp3PlayerException Thrown either when the file is not found, or is not valid mp3,
	 * or when the Player could not be created
	 * @exception InterruptedException 
	 */
	public abstract boolean play(Song song) throws FileNotFoundException, Mp3PlayerException, InterruptedException;

	/**
	 * Stops the current play
	 */
	public abstract void stop();
}
