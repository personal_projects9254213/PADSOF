package es.uam.eps.sadp.p3;

import java.io.Serializable;
import java.util.*;

/**
 * Class Playlist
 * @author <a href="mailto:nicolas.serranos@estudiante.uam.es">Nicolas Serrano</a>
 * @author <a href="mailto:miguela.luque@estudiante.uam.es">Miguel Angel Luque</a>
 * @author <a href="mailto:blanca.mercado@estudiante.uam.es">Blanca Mercado</a>
 * Practice group: 2291
 */
public class Playlist implements Serializable {
	private static final long serialVersionUID = 1L;
	private String title;
	private List<Song> songs = new ArrayList<>();
	private Registered parent;

	/**
	 * Constructor of the class playlist
	 * @param title title of the album
	 * @param song first song added to the playlist
	 * @param parent user creating the playlist
	 */
	public Playlist(String title, Song song, Registered parent) {
		this.title = title;
		songs.add(song);
		this.parent = parent;
		parent.setPlaylists(this);
	}
	public Playlist(String title, Album album, Registered parent) {
		this.title = title;
		addToPlaylist(album);
		this.parent = parent;
		parent.setPlaylists(this);
	}
	public Playlist(String title, Playlist playlist, Registered parent) {
		this.title = title;
		addToPlaylist(playlist);
		this.parent = parent;
		parent.setPlaylists(this);
	}

	public String getTitle() {
		return title;
	}

	public List<Song> getSongs() {
		return songs;
	}

	/**
	 * Add a song to the playlist
	 * @param song song added to the playlist
	 */
	public void addToPlaylist(Song song) {
		songs.add(song);
	}

	/**
	 * Add an album to the playlist
	 * @param album added to the playlist
	 */
	public void addToPlaylist(Album album) {
		for(Song current: album.getSongs()){
			if(!songs.contains(current)){
				songs.add(current);
			}
		}
	}

	/**
	 * Add a playlist to the playlist
	 * @param playlist added to the playlist
	 */
	public void addToPlaylist(Playlist playlist) {
		for(Song current: playlist.getSongs()){
			if(!songs.contains(current)){
				songs.add(current);
			}
		}
	}

	/**
	 * Removes a song from the playlist. If after removing
	 * the song the playlist is empty, removes the playlist
	 * @param song removed song
	 */
	public void quitFromPlaylist(Song song) {
		songs.remove(song);
		if(songs.isEmpty()){
			parent.deletePlaylist(this);
		}
	}

	@Override
	public String toString() {
		return title;
	}
}
