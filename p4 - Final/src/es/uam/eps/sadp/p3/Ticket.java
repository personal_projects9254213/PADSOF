package es.uam.eps.sadp.p3;

import java.io.Serializable;

/**
 * Class Ticket
 * @author <a href="mailto:nicolas.serranos@estudiante.uam.es">Nicolas Serrano</a>
 * @author <a href="mailto:miguela.luque@estudiante.uam.es">Miguel Angel Luque</a>
 * @author <a href="mailto:blanca.mercado@estudiante.uam.es">Blanca Mercado</a>
 * Practice group: 2291
 */
public class Ticket implements Serializable {
	private static final long serialVersionUID = 1L;
	private TicketType type;
	private Registered user;
	private Song song;
	private Album album;
	private String content;

	/**
	 * Enumeration TicketType defines the types of ticket states
	 */
	public enum TicketType {
		REPORT,
		VALIDATION,
		NOTIFICATION;
	}

	/**
	 * Constructor of the class Ticket
	 * @param type type of ticket
	 * @param song song described in ticket
	 */
	public Ticket(TicketType type, Song song) {
		this.type = type;
		this.song = song;
		content = "song";
	}

	/**
	 * Constructor of the class Ticket
	 * @param type type of ticket
	 * @param album album described in ticket
	 */
	public Ticket(TicketType type, Album album) {
		this.type = type;
		this.album = album;
		content = "album";
	}

	/**
	 * Constructor of the class Ticket
	 * @param type type of ticket
	 * @param song song described in ticket
	 * @param user user creating the ticket
	 */
	public Ticket(TicketType type, Song song, Registered user) {
		this.type = type;
		this.song = song;
		this.user = user;
	}

	public TicketType getType() {
		return type;
	}
	public Song getSong() {
		return song;
	}
	public Album getAlbum() {
		return album;
	}
	public Registered getUser() {
		return user;
	}
	public String getContent() {
		return content;
	}

	/**
	 * Get message of validation tickets
	 * @return string message required
	 */
	private String getValidationInfo() {
		if(song != null) {
			return song.getTitle();
		} else if (album != null) {
			return album.getTitle();
		}
		return null;
	}

	/**
	 * Get message of notification tickets
	 * @return string message required
	 */
	private String getNotificationInfo() {
		if(song != null) {
			return song.getAuthor().getNick() + " just uploaded the song " + song.getTitle();
		} else if (album != null) {
			return album.getAuthor().getNick() + " just uploaded the album " + album.getTitle();
		}
		return null;
	}

	/**
	 * Get message of report tickets
	 * @return string message required
	 */
	private String getReportInfo() {
		return user.getNick() + " reported " + song.getTitle();
	}

	@Override
	public String toString(){
		if(type == TicketType.VALIDATION) {
			return getValidationInfo();
		} else if (type == TicketType.REPORT) {
			return getReportInfo();
		} else if (type == TicketType.NOTIFICATION) {
			return getNotificationInfo();
		}
		return null;
	}
}
