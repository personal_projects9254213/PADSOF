package es.uam.eps.sadp.p3;
import java.time.*;
import java.util.*;
import java.io.*;

import org.apache.commons.io.FileUtils;

import es.uam.eps.sadp.p3.Song.ValidationType;
import pads.musicPlayer.exceptions.Mp3PlayerException;

/**
 * Class System it is defined as a singleton
 * @author <a href="mailto:nicolas.serranos@estudiante.uam.es">Nicolas Serrano</a>
 * @author <a href="mailto:miguela.luque@estudiante.uam.es">Miguel Angel Luque</a>
 * @author <a href="mailto:blanca.mercado@estudiante.uam.es">Blanca Mercado</a>
 * Practice group: 2291
 */
public class System implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final String saveFolder = "./save/";
	private static final Admin admin = new Admin("Rex", "SuperSecret");
	private static System app = null;
	private List<Ticket> tickets;
	private List<Registered> registeredUsers;
	private List<Song> songs;
	private List<Album> albums;
	private Unregistered defaultUser;
	private User currentUser;

	/**
	 * Get the instance for the singleton System. If there is a savefile
	 * of the system it loads it, else it creates a new system.
	 * @return the singleton System
	 */
	public static System getInstance(){
		File archivo = new File(saveFolder+"data.txt");
		if(app == null){
			if (archivo.exists()) {
				return System.load();
			}
			app = new System();
		}

		return app;
	}

	/**
	 * Loads the system from a file and saves the system
	 * @return the singleton System
	 */
	private static System load(){
		try{
			File saveFile = new File(saveFolder+"data.txt");
			FileInputStream saveStream = new FileInputStream(saveFile);
			ObjectInputStream inputStream = new ObjectInputStream(saveStream);
			app = (System) inputStream.readObject();
			saveStream.close();
			inputStream.close();
			return app;
		} catch(FileNotFoundException e) {
			e.printStackTrace();
			return null;
		} catch(IOException e) {
			e.printStackTrace();
			return null;
		} catch(ClassNotFoundException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Tells if the current user is an admin
	 * @return true if the current user is an admin
	 */
	public boolean isAdmin() {
		if(currentUser == admin) {
			return true;
		}else {
			return false;
		}
	}

	/**
	 * Tells if the current user is premium
	 * @return true if the current user is premium
	 */
	public boolean isPremium() {
		if(currentUser == admin) {
			return false;
		} else if(currentUser == defaultUser) {
			return false;
		} else {
			Registered r = (Registered) currentUser;
			return r.getPremium();
		}
	}

	public boolean isDefaultUser() {
		if(currentUser == defaultUser) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Deletes the syste savefile
	 * @return boolean telling if was correctly done
	 */
	public static boolean deleteSaveFile() {
		File saveFile = new File(saveFolder+"data.txt");
		if (!saveFile.exists()) {
			return false;
		} else {
			saveFile.delete();
			if(saveFile.exists()) {
				return false;
			}
			return true;
		}
	}

	/**
	 * Constructor of the class System
	 */
	private System() {
		tickets = new ArrayList<>();
		registeredUsers = new ArrayList<>();
		songs = new ArrayList<>();
		albums = new ArrayList<>();
		defaultUser = new Unregistered();
		currentUser = defaultUser;
	}

	/**
	 * Gets the ticket of the currentUser (Admin or Registered)
	 * @return list of all user tickets
	 */
	public List<Ticket> getTickets(){
		if(currentUser == admin) {
			return tickets;
		} else if(currentUser != defaultUser) {
			return ((Registered) currentUser).getNotifications();
		}
		return null;
	}

	/**
	 * Register a new user in the system
	 * @param name name of the user
	 * @param nick nick of the User
	 * @param password password of the User
	 * @param dateOfBirth date of birth of the user
	 * @return boolean telling if was correctly done
	 */
	public boolean register(String name, String nick, String password, LocalDate dateOfBirth) {
		if(name == null || nick == null || password == null || dateOfBirth == null) {
			return false;
		}
		return registeredUsers.add(new Registered(name, nick, password, dateOfBirth));
	}

	/**
	 * Log to the system
	 * @param id nick of the User
	 * @param password password of the User
	 * @return boolean telling if was correctly done
	 */
	public boolean login(String id, String password) {
		if(admin.getNick().equals(id) && password.hashCode() == admin.getPassword()){
			currentUser = admin;
			return true;
		} else {
			for(Registered current: registeredUsers){
				if(current.getNick().equals(id) && password.hashCode() == current.getPassword()){
					if(!current.isBanned()) {    //If the user is banned he can't login
						currentUser = current;
						return true;
					}
					return false;
				}
			}
		}
		return false;
	}

	/**
	 * Discconect from the system
	 * @return boolean telling if was correctly done
	 */
	public boolean disconnect(){
		User.player.stop();
		try {
			currentUser = defaultUser;
			save();
			return true;
		} catch(FileNotFoundException e1) {
			java.lang.System.out.println("System savefile missing!");
			return false;
		} catch(IOException e2) {
			java.lang.System.out.println("System could have not be saved properly");
			return false;
		}
	}

	/**
	 * Saves the system in a file
	 * @exception FileNotFoundException if save folder does not exist.
	 * @exception IOException problem while saving
	 */
	public void save() throws FileNotFoundException, IOException  {
		File saveFile = new File(saveFolder+"data.txt");
		saveFile.createNewFile(); // if file already exists will do nothing
		FileOutputStream saveStream = new FileOutputStream(saveFile);
		ObjectOutputStream outputStream = new ObjectOutputStream(saveStream);
		outputStream.writeObject(app);
		saveStream.close();
		outputStream.close();
	}

	/**
	 * Search from the system songs a song title containing method's argument
	 * @param busqueda title of the song wanted to search
	 * @return list of songs matching the result
	 */
	public List<Song> searchSong(String busqueda) {
		List<Song> searchedSongs = new ArrayList<>();
		for(Song current: songs){
			if(current.getTitle().contains(busqueda)){
				if((!current.isExplicit() && current.isValidate()) || currentUser == admin){
					searchedSongs.add(current);
				}
				else if(current.isExplicit() && currentUser != defaultUser &&
						((Registered) currentUser).isOverage() && current.isValidate()){
					searchedSongs.add(current);
				}
			}
		}
		return searchedSongs;
	}

	/**
	 * Search from the system albums an album title containing method's argument
	 * @param busqueda title of the album wanted to search
	 * @return list of albums matching the result
	 */
	public List<Album> searchAlbum(String busqueda) {
		List<Album> searchedAlbums = new ArrayList<>();
		for(Album current: albums){
			if(current.getTitle().contains(busqueda)){
				searchedAlbums.add(current);
			}
		}
		return searchedAlbums;
	}

	/**
	 * Search from the system registereds a registered nick containing method's argument
	 * @param busqueda nick of the registered wanted to search
	 * @return list of registereds matching the result
	 */
	public List<Registered> searchProfile(String busqueda) {
		List<Registered> searchedProfiles = new ArrayList<>();
		for(Registered current: registeredUsers){
			if(current.getNick().contains(busqueda)){
				searchedProfiles.add(current);
			}
		}
		return searchedProfiles;
	}

	/**
	 * Delete album from the system
	 * @param album album wanted to delete from the system
	 * @return boolean telling if was correctly done
	 */
	public boolean delete(Album album) {
		if(currentUser == admin){
			removeAlbum(album);
			return true;
		} else if(album.getAuthor() == currentUser) {
			removeAlbum(album);
			return true;
		}
		return false;
	}

	/**
	 * Delete album from the system
	 * @param album album wanted to delete from the system
	 */
	private void removeAlbum(Album album){
		while(!album.getSongs().isEmpty()){
			deleteSongFromSystem(album.getSongs().get(0));
		}
		album.getAuthor().getAlbums().remove(album);
		albums.remove(album);
	}

	/**
	 * Delete song from the system
	 * @param song song wanted to delete from the system
	 * @return boolean telling if was correctly done
	 */
	public boolean delete(Song song) {
		if(currentUser == admin){
			deleteSongFromSystem(song);
			song.getAuthor().getSongs().remove(song);
			return true;
		} else if(song.getAuthor() == currentUser){
			deleteSongFromSystem(song);
			song.getAuthor().getSongs().remove(song);
			return true;
		}
		return false;
	}

	/**
	 * Delete song from the system
	 * @param song song wanted to delete from the system
	 */
	private void deleteSongFromSystem(Song song){
		File songFile = new File(song.getPath());
		songs.remove(song);
		songFile.delete();
		if(song.getAlbum() != null) {
			song.getAlbum().deleteSong(song);
		}
		for(Registered currentRegistered: registeredUsers){
			for(Playlist currentPlaylist: currentRegistered.getPlaylists()){
				currentPlaylist.quitFromPlaylist(song);
			}
		}
	}

	/**
	 * Upload an album
	 * @param albumTitle title of the album
	 * @param albumSongs Map with the paths and titles of the songs in album
	 * @return boolean telling if was correctly done
	 */
	public boolean upload(String albumTitle, Map<String, String> albumSongs){
		Album newAlbum;
		if(currentUser == admin || currentUser == defaultUser){
			return false;
		}

		newAlbum = new Album(albumTitle, (Registered) currentUser);

		for(String path: albumSongs.keySet()){
			Song creatingSong;
			creatingSong = uploadSong(path, albumSongs.get(path), newAlbum);
			if(creatingSong == null) {
				return false;
			}
			newAlbum.addSong(creatingSong);
		}
		albums.add(newAlbum);
		tickets.add(new Ticket(Ticket.TicketType.VALIDATION, newAlbum));
		return true;

	}

	/**
	 * Upload a single
	 * @param path path of the song
	 * @param songTitle title of the song
	 * @return boolean telling if was correctly done
	 */
	public boolean upload(String path, String songTitle) {
		Song newSong;
		if(currentUser == admin || currentUser == defaultUser){
			return false;
		}
		newSong = uploadSong(path, songTitle, null);

		if(newSong == null) {
			return false;
		}

		tickets.add(new Ticket(Ticket.TicketType.VALIDATION, newSong));
		return true;
	}

	/**
	 * Upload a song
	 * @param path path of the song
	 * @param songTitle title of the song
	 * @param album album of the song
	 * @return song uploaded
	 */
	private Song uploadSong(String path, String songTitle, Album album) {
		String newPath;
		Song newSong;
		if(path == null || songTitle == null) {
			return null;
		}

		if(album == null) {
			newPath = copyFile(path, currentUser.getNick());
		} else {
			newPath = copyFile(path, currentUser.getNick()+"/"+album.getTitle());
		}

		newSong = new Song(newPath, songTitle, (Registered) currentUser, album);
		songs.add(newSong);
		return newSong;
	}

	/**
	 * Reupload a song
	 * @param song wanted to reupload
	 * @return boolean telling if was correctly done
	 */
	public boolean reupload(Song song) {
		LocalDate checkDate = song.getRejectDate();
		if(songs.contains(song) && song.getAuthor() == currentUser && song.getValidate() == ValidationType.FALSE) {
			if(checkDate.plusDays(3).isAfter(LocalDate.now())) {
				song.setValidate(ValidationType.INPROGRESS);
				tickets.add(new Ticket(Ticket.TicketType.VALIDATION, song));
				return true;
			}
		}
		return false;
	}

	/**
	 * Validate Song or Album
	 * We don't know if we are validating a Song or and Album inside the function
	 * But we can see it at the user interface
	 * @param ticket ticket of an uploading song or album
	 * @param valid boolean if a song is valid or not
	 * @param explicit boolean if a song is explicit or not
	 * @return boolean telling if was correctly done
	 */
	public boolean validate(Ticket ticket, Boolean valid, Boolean explicit) {
		Song validatingSong;
		Album validatingAlbum;
		if(currentUser == admin && ticket.getType() == Ticket.TicketType.VALIDATION) {
			tickets.remove(ticket);
			validatingSong = ticket.getSong();
			validatingAlbum = ticket.getAlbum();
			if(validatingSong != null) {
				validatingSong.validate(valid, explicit, LocalDate.now());
				validatingSong.getAuthor().notifyFollowers(validatingSong);
				return true;
			} else if(validatingAlbum != null) {
				validatingAlbum.validate(valid, explicit, LocalDate.now());
				validatingAlbum.getAuthor().notifyFollowers(validatingAlbum);
				return true;
			}
		}
		return false;
	}

	/**
	 * Copy a file into another directory
	 * @param originalPath path of the file you want to copy
	 * @param destinationPath directory from the file you want to copy
	 * @return new file path
	 */
	private String copyFile(String originalPath, String destinationPath){
		File originFile = new File(originalPath);
		File destinationDir = new File(saveFolder+"/"+destinationPath);
		try {
			FileUtils.copyFileToDirectory(originFile, destinationDir);
			return destinationDir.getPath()+"/"+originFile.getName();
		} catch(IOException e){
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Report a song
	 * @param song wanted to reupload
	 * @return boolean telling if was correctly done
	 */
	public boolean report(Song song){
		if(currentUser != admin  && currentUser != defaultUser && song.isValidate()) {
			tickets.add(new Ticket(Ticket.TicketType.REPORT, song, ((Registered) currentUser)));
			return true;
		}
		return false;
	}

	/**
	 * Check a report
	 * @param ticket ticket of a reported song
	 * @param bool boolean if ban or not the user
	 * @return boolean telling if was correctly done
	 */
	public boolean checkReport(Ticket ticket, boolean bool){
		if(currentUser == admin && ticket.getType() == Ticket.TicketType.REPORT) {
			tickets.remove(ticket);
			if(bool) {
				ticket.getSong().getAuthor().permaBan();
				delete(ticket.getSong());
			} else {
				ticket.getUser().temporalBan();
			}
			return true;
		}
		return false;
	}


	/**
	 * Plays the song passed by argument
	 * @param song song wanted to play
	 * @return boolean telling if was correctly done
	 */
	public boolean play(Song song) {
		try {
			if(currentUser == admin || currentUser == song.getAuthor()) {
				return currentUser.play(song);
			} else if (song.isValidate()) {
				return currentUser.play(song);
			}
			return false;
		} catch(FileNotFoundException e1) {
			deleteSongFromSystem(song); //If the file is not found, the song is removed from the system.
			java.lang.System.out.println(song+ "not founded");
			return false;
		} catch(Mp3PlayerException e2) {
			e2.printStackTrace();
			return false;
		} catch(InterruptedException e3) {
			e3.printStackTrace();
			return false;
		}
	}

	/**
	 *stops the current play
	 */
	public void stop() {
		currentUser.stop();
	}

	/**
	 * Follow another registered
	 * @param user registered wanted to be followed
	 * @return boolean telling if was correctly done
	 */
	public boolean follow(Registered user) {
		if(currentUser != admin && currentUser != defaultUser) {
			return ((Registered) currentUser).follow(user);
		}
		return false;
	}

	/**
	 * Unfollow another registered
	 * @param user registered wanted to be unfollowed
	 * @return boolean telling if was correctly done
	 */
	public boolean unfollow(Registered user) {
		if(currentUser != admin && currentUser != defaultUser) {
			return ((Registered) currentUser).unfollow(user);
		}
		return false;
	}

	/**
	 * Become premium by paying
	 * @param creditCard creditCard number
	 * @return boolean telling if was correctly done
	 */
	public boolean becomePremium(String creditCard) {
		if(currentUser != admin && currentUser != defaultUser) {
			return ((Registered) currentUser).becomePremium(creditCard);
		}
		return false;
	}

	/**
	 * Return current user logged
	 * @return current user registered
	 */
	public User currentUser() {
		return currentUser;
	}

	/**
	 * Become premium by achieving reproductions milestone
	 * @return boolean telling if was correctly done
	 */
	public boolean becomePremium() {
		if(currentUser != admin && currentUser != defaultUser) {
			return ((Registered) currentUser).becomePremium();
		}
		return false;
	}

}
