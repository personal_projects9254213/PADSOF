package es.uam.eps.sadp.p3;

import java.time.*;
import java.util.*;

import es.uam.eps.padsof.telecard.FailedInternetConnectionException;
import es.uam.eps.padsof.telecard.InvalidCardNumberException;
import es.uam.eps.padsof.telecard.OrderRejectedException;
import es.uam.eps.padsof.telecard.TeleChargeAndPaySystem;

import java.io.FileNotFoundException;
import java.io.Serializable;

import pads.musicPlayer.Mp3Player;
import pads.musicPlayer.exceptions.Mp3PlayerException;

/**
 * Class Registered.
 * @author <a href="mailto:nicolas.serranos@estudiante.uam.es">Nicolas Serrano</a>
 * @author <a href="mailto:miguela.luque@estudiante.uam.es">Miguel Angel Luque</a>
 * @author <a href="mailto:blanca.mercado@estudiante.uam.es">Blanca Mercado</a>
 * Practice group: 2291
 */
public class Registered extends Unregistered implements Serializable{

	/**
	 * Enumeration BanType defines the types of ban states.
	 */
	public enum BanType {

		/** The none. */
		NONE,

		/** The temporal. */
		TEMPORAL,

		/** The permanent. */
		PERMANENT;
	}

	/**
	 * Subclass Premium defines the information for premium state.
	 */
	private static class Premium implements Serializable {
		private static final long serialVersionUID = 1L;
		private boolean premium;
		private LocalDate expiration;
	}

	/**
	 * Subclass Ban defines the information for ban state.
	 */
	private static class Ban implements Serializable {
		private static final long serialVersionUID = 1L;
		private BanType banType;
		private LocalDate expiration;
	}

	private static final long serialVersionUID = 1L;
	private final static int MILESTONE = 1000;
	private List<Registered> followeds = new ArrayList<>();
	private List<Registered> followers = new ArrayList<>();
	private List<Song> queue = new ArrayList<>();
	private List<Song> songs = new ArrayList<>();
	private List<Album> albums = new ArrayList<>();
	private List<Ticket> notification = new ArrayList<>();
	private List<Playlist> playlists = new ArrayList<>();
	private String name;
	private String description;
	private LocalDate dateOfBirth;
	private Premium premium = new Premium();
	private Ban banned = new Ban();

	/**
	 * Constructor of the class registered.
	 * @param name name of the user
	 * @param nick nick of the user
	 * @param password password of the user
	 * @param dateOfBirth date of birth of the user
	 */
	public Registered(String name, String nick, String password, LocalDate dateOfBirth) {
		super(nick, password);
		this.name = name;
		this.dateOfBirth = dateOfBirth;
		this.premium.premium = false;
		this.banned.banType = BanType.NONE;
	}

	/**
	 * Gets the number of listened songs or 0 if registered is premium
	 * @return the number of songs listened
	 */
	@Override
	public int getListenedSongs() {
		if(getPremium()) {
			return 0;
		} else {
			return super.getListenedSongs();
		}
	}

	/**
	 * Gets the followers
	 * @return the list of followers
	 */
	public List<Registered> getFollowers() {
		return followers;
	}

	/**
	 * Gets the queue of songs
	 * @return the queue
	 */
	public List<Song> getQueue() {
		return queue;
	}

	/**
	 * Gets the songs
	 * @return list of songs
	 */
	public List<Song> getSongs() {
		return songs;
	}

	/**
	 * Gets the albums
	 * @return list of albums
	 */
	public List<Album> getAlbums() {
		return albums;
	}

	/**
	 * Gets the notifications
	 * @return list of notifications
	 */
	public List<Ticket> getNotifications() {
		return notification;
	}

	/**
	 * Gets the playlists.
	 * @return all the playlists
	 */
	public List<Playlist> getPlaylists() {
		return playlists;
	}

	public Playlist getPlaylist(String s) {
		for(Playlist p: playlists) {
			if(p.getTitle().equals(s)) {
				return p;
			}
		}
		return null;
	}
	/**
	 * Gets the description
	 * @return description of the profile
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Gets the premium.
	 * @return true if premium or false if not
	 */
	public boolean getPremium() {
		if (!this.premium.premium) {
			return false;
		} else if (LocalDate.now().compareTo(this.premium.expiration) > 0) {
			this.premium.premium = false;
			return false;
		} else {
			return true;
		}
	}

	/**
	 * Gets the monthly progress of the user.
	 * @return the progress
	 */
	public long getProgress() {
		long progress = 0;
		for (Song s : this.songs) {
			progress += s.getReproductions();
		}
		for (Album a : this.albums) {
			for(Song s : a.getSongs()) {
				progress += s.getReproductions();
			}
		}
		return progress;
	}

	/**
	 * Checks if is banned.
	 * @return true if the user is banned or false if not
	 */
	public boolean isBanned() {
		if (banned.banType == BanType.NONE) {
			return false;
		} else if ((banned.banType == BanType.TEMPORAL) && (LocalDate.now().compareTo(banned.expiration) > 0)) {
			banned.banType = BanType.NONE;
			return false;
		} else {
			return true;
		}
	}

	/**
	 * Checks if is overage.
	 * @return true if the user is overge or false if not
	 */
	public boolean isOverage() {
		return ((LocalDate.now().compareTo(dateOfBirth.plusYears(18)) >= 0));
	}

	/**
	 *  Sets a playlist
	 * @param playlist playlist to set
	 */
	public void setPlaylists(Playlist playlist) {
		this.playlists.add(playlist);
	}

	/**
	 * Enters a new song in the songs register
	 * @param s: song to be added to the songs register
	 */
	public void addSong(Song s){
		this.songs.add(s);
	}

	/**
	 * Removes a song in from play queue
	 * @param s: song to be removed from the queue
	 */
	public void quitQueueSong(Song s){
		this.queue.remove(s);
	}

	/**
	 * Removes a song from the songs register
	 * @param s: song to be removed from the songs register
	 */
	public void quitSong(Song s){
		this.songs.remove(s);
	}

	/**
	 * Gets the first song of the queue and erases it from the list.
	 * @return first song in the queue
	 */
	public Song extractFirstSong() {
		Song s;
		if (queue.size() < 1) {
			return null;
		}else {
			s = queue.get(0);
			queue.remove(0);
			return s;
		}
	}


	/**
	 * Become premium by paying
	 * @return false if the user was already premium or if the card was not valid
	 */
	public boolean becomePremium(String creditCard) {

		if(getPremium()) {
			return false;
		} else {
			if(TeleChargeAndPaySystem.isValidCardNumber(creditCard)) {
				try {
					TeleChargeAndPaySystem.charge(creditCard, "premium", 5);
					this.premium.premium = true;
					this.premium.expiration = LocalDate.now().plusDays(30);
					return true;
				} catch(InvalidCardNumberException e) {
					e.printStackTrace();

				} catch(FailedInternetConnectionException e) {
					e.printStackTrace();

				} catch(OrderRejectedException e) {
					e.printStackTrace();
				}
			}
			return false;
		}

	}

	/**
	 * Become premium for free
	 * @return false if the user was already premium
	 */
	public boolean becomePremium() {
		if(getProgress() >= MILESTONE) {
			this.premium.premium = true;

			this.premium.expiration = LocalDate.now().plusDays(30);

			return true;

		}
		return false;
	}


	/**
	 * Delete playlist.
	 * @param playlist: the playlist to be deleted
	 */
	public void deletePlaylist(Playlist playlist) {
		playlists.remove(playlist);
	}

	/**
	 * Adds a notification to the current user telling him about the release of a new song
	 * @param s : song which the notification is about
	 */
	public void addNotification(Song s) {
		notification.add(new Ticket(Ticket.TicketType.NOTIFICATION, s));
	}

	/**
	 * Adds a notification to the current user telling him about the release of a new album
	 * @param a : album which the notification is about
	 */
	public void addNotification(Album a) {
		notification.add(new Ticket(Ticket.TicketType.NOTIFICATION, a));
	}

	/**
	 * Notifies the followers of the current user bout the release of his new song
	 * @param s : song which the notification is about
	 */
	public void notifyFollowers(Song s) {
		for (Registered f : followers) {
			f.addNotification(s);
		}
		this.songs.add(s);
	}

	/**
	 * Notifies the followers of the current user bout the release of his new album
	 * @param a : album which the notification is about
	 */
	public void notifyFollowers(Album a) {
		for (Registered f : followers) {
			f.addNotification(a);
		}
		this.albums.add(a);
	}

	/**
	 * Follows a user
	 * @param followed : user to follow
	 */
	public boolean follow(Registered followed) {
		if(followeds.contains(followed)) {
			return false;
		}
		this.followeds.add(followed);
		followed.followers.add(this);
		return true;
	}

	/**
	 * checks if he is following a user
	 * @param followed : user to follow
	 * @return true if is following him
	 */
	public boolean isFollowing(Registered followed) {
		if(followeds.contains(followed)) {
			return true;
		}
		return false;
	}

	/**
	 * Unfollows a user
	 * @param followed : user to unfollow
	 */
	public boolean unfollow(Registered followed) {
		if(!followeds.contains(followed)) {
			return false;
		}
		followeds.remove(followed);
		followed.followers.remove(this);
		return true;
	}

	/**
	 * Adds a song to the queue of an user
	 * @param the song
	 */
	public void addQueueSong(Song s){
		this.queue.add(s);
	}

	/**
	 * Bans the current user permanently
	 */
	public void permaBan() {
		if(banned.banType != BanType.PERMANENT) {
			banned.banType = BanType.PERMANENT;
		}
	}

	/**
	 * Bans the current user for a month
	 */
	public void temporalBan() {
		if(banned.banType != BanType.PERMANENT) {
			banned.banType = BanType.TEMPORAL;
			banned.expiration = LocalDate.now().plusDays(30);
		}
	}

	/**
	 * Plays the song passed by argument
	 * @param song song wanted to play
	 * @return boolean saying if the song have been reproducted
	 * @exception FileNotFoundException if file does not exist.
	 * @exception Mp3PlayerException Thrown either when the file is not found, or is not valid mp3,
	 * or when the Player could not be created
	 * @exception InterruptedException
	 */
	@Override
	public boolean play(Song song) throws FileNotFoundException, Mp3PlayerException, InterruptedException {
		player.stop();
		player = new Mp3Player();
		if(!Mp3Player.isValidMp3File(song.getPath()) ||song.isExplicit()){
			return false;
		} else if((getListenedSongs() < MAXSONGS)||(this.getPremium())) {
			setListenedSongs(getListenedSongs()+1);
			song.setReproductions(song.getReproductions()+1);
			player.add(song.getPath());
			player.play();
			return true;
		} else {
			return false;
		}
	}
	/**
	 * Plays the playlist by argument starting by the selected song
	 * @param song song wanted to play
	 * @return boolean saying if the song have been reproducted
	 * @exception FileNotFoundException if file does not exist.
	 * @exception Mp3PlayerException Thrown either when the file is not found, or is not valid mp3,
	 * or when the Player could not be created
	 * @exception InterruptedException
	 */
	public boolean play(Playlist p,Song song) throws FileNotFoundException, Mp3PlayerException, InterruptedException {
		boolean start = false;
		boolean out = true;
		player = new Mp3Player();
		player.stop();
		for(Song s: p.getSongs()){
			if(s.equals(song)){
				start = true;
			}
			if(start){
				if(!Mp3Player.isValidMp3File(song.getPath()) ||song.isExplicit()){
					out = false;
				} else if((getListenedSongs() < MAXSONGS)||(this.getPremium())) {
					setListenedSongs(getListenedSongs()+1);
					song.setReproductions(song.getReproductions()+1);
					player.add(s.getPath());
				}
			}
		}
		player.play();
		return out;
	}
	/**
	 * Stops the current play
	 */
	public void stop() {
		player.stop();
	}
	@Override
	public String toString(){
		return getNick();
	}

	/**
	 * Delete Notification
	 * @param not notification wanted to be deleted
	 */
	public void deleteNotification(Ticket not){
		notification.remove(not);
	}

}
