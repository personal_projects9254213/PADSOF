package es.uam.eps.sadp.p3;

import java.util.*;
import java.io.Serializable;
import java.time.*;

/**
 * Class Album
 * @author <a href="mailto:nicolas.serranos@estudiante.uam.es">Nicolas Serrano</a>
 * @author <a href="mailto:miguela.luque@estudiante.uam.es">Miguel Angel Luque</a>
 * @author <a href="mailto:blanca.mercado@estudiante.uam.es">Blanca Mercado</a>
 * Practice group: 2291
 */
public class Album implements Serializable {
	private static final long serialVersionUID = 1L;
	private String title;
	private Registered author;
	private List<Song> songs = new ArrayList<>();

	/**
	 * Constructor of the class album
	 * @param title title of the album
	 * @param author author of the album
	 */
	public Album(String title, Registered author) {
		this.title = title;
		this.author = author;
	}

	public String getTitle() {
		return title;
	}

	public Registered getAuthor() {
		return author;
	}

	public List<Song> getSongs() {
		return songs;
	}

	/**
	 * Add a song to the albums
	 * @param song song you want to add
	 */
	public void addSong(Song song) {
		songs.add(song);
	}

	/**
	 * Deletes a song of the albums. If after removing
	 * the song the album is empty, removes the album
	 * @param song song you want to add
	 */
	public void deleteSong(Song song) {
		songs.remove(song);
		if(songs.isEmpty()){
			System.getInstance().delete(this);
		}
	}

	/**
	 * Validate all the songs of an album
	 * @param valid boolean if it is valid or not
	 * @param explicit boolean if it is explicit or not
	 * @param rejectDate date when they are validated
	 */
	public void validate(Boolean valid, Boolean explicit, LocalDate rejectDate) {
		for (Song songs : this.songs) {
			songs.validate(valid, explicit, rejectDate); /** all the songs inside an album which is explicit are explicit */
		}
	}

	@Override
	public String toString(){
		return title+" "+author.getNick();
	}

}
