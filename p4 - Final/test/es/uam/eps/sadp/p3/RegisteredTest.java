package es.uam.eps.sadp.p3;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

/**
 * Class Registered test
 * @author <a href="mailto:nicolas.serranos@estudiante.uam.es">Nicolas Serrano</a>
 * @author <a href="mailto:miguela.luque@estudiante.uam.es">Miguel Angel Luque</a>
 * @author <a href="mailto:blanca.mercado@estudiante.uam.es">Blanca Mercado</a>
 * Practice group: 2291
 */
public class RegisteredTest{
	private Registered user;
	private Registered fo;
	private Playlist play;
    private Song song1;
    private Song song2;
	private Playlist play2;
	private Registered user2;

	@Before
	public void setUp() {
		fo = new Registered("name1", "nick1", "password1", LocalDate.of(1999,6,28));
		user = new Registered("Jake", "Weik", "woke", LocalDate.of(2010,4,7));
		song1 = new Song("./testSong/song1.mp3","goodbunny",fo);
		song2 = new Song("./testSong/song2.mp3","goodbunny",fo);
		play2 = new Playlist("playlist1", song1, user);
		play = new Playlist("playlist1", song1, user);
		user2 = new Registered("Blanca", "Wait", "white", LocalDate.of(1999,4,7));


	}
	
	@Test
	public void testPlay() {
		song1.validate(true, true, LocalDate.of(1999,6,28));
		song2.validate(true, false, LocalDate.of(2007,4,9));
		user.setListenedSongs(Unregistered.MAXSONGS - 1);
		try{
			assertFalse(user.play(song1));
			assertTrue(user.play(song2));
			assertFalse(user.play(song2));
		}
		catch(Exception e){
		}
	}
	
	@Test
	public final void testGetPlaylists() {
		List<Playlist> p = new ArrayList<>();
		play = new Playlist("playlist1", song1, user2);
		p.add(play);
		assertEquals(user2.getPlaylists(),p);
	}

	@Test
	public final void testGetProgress() {
		user2.addSong(song1);
		song1.setReproductions(3);
		assertTrue(user2.getProgress() == 3);
	}

	@Test
	public final void testAddQueueSong() {
		user.addQueueSong(song2);
		List<Song> queue = new ArrayList<>();
		queue.add(song2);
		assertEquals(queue,user.getQueue());
	}

	@Test
	public final void testExtractFirstSong() {
		List<Song> queue = new ArrayList<>();
		user.addQueueSong(song1);
		user.addQueueSong(song2);
		user.extractFirstSong();
		queue.add(song2);
		assertEquals(queue,user.getQueue());
	}

	@Test
	public final void testGetPremium() {
		assertTrue(user.getPremium() == false);
	}

	@Test
	public final void testBecomePremium() {
		assertFalse(user.becomePremium());
		assertFalse(user.becomePremium("356600202036050"));
		assertTrue(user.becomePremium("3566002020360508"));
	}

	@Test
	public final void testIsBanned() {
		assertTrue(user.isBanned() == false);
	}

	@Test
	public final void testIsOverage() {
		assertFalse(user.isOverage());
		assertTrue(fo.isOverage());
	}

	@Test
	public final void testDeletePlaylist() {
		List<Playlist> p = new ArrayList<>();
		user.deletePlaylist(play);
		p.add(play2);
		assertEquals(user.getPlaylists(),p);
	}

	@Test
	public final void testAddNotificationSong() {
		user.addNotification(song2);
		assertNotNull(user2.getNotifications());
		
	}

	@Test
	public final void testAddNotificationAlbum() {
		Album a = new Album("title", user2);
		user2.addNotification(a);
		assertNotNull(user2.getNotifications());
	}

	@Test
	public final void testNotifyFolowersSong() {
		user2.follow(user);
		user.notifyFollowers(song2);
		assertNotNull(user.getNotifications());		
	}

	@Test
	public final void testNotifyFolowersAlbum() {
		user2.follow(user);
		Album a = new Album("title", user);
		user.notifyFollowers(a);
		assertNotNull(user.getNotifications());
	}

	@Test
	public final void testUnfollow() {
		List<Registered> followers = new ArrayList<>();
		user2.follow(user);
		user2.unfollow(user);
		assertEquals(user2.getFollowers(), followers);
	}

}
